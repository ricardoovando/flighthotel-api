/**
 * MailingController.js
 *
 * @description :: Server-side logic for managing flights API requests
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
"use strict";

const NAME = "MailingController";

var sendMailService = require('cocha-external-services').sendMail;

function* getSentInfo() {
	this.body = {
		msg: yield new Promise((resolve, reject) => {
			sendMailService.getInfoSentByID(this.params.id, function (err, result) {
				if (err) {
					Koa.log.error(err);
					reject(err);
				} else {
					resolve(result);
				}
			});
		})
	};
}

module.exports = {
	getSentInfo: getSentInfo
};
