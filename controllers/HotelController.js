/**
 * HotelDetailController.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let flightAndHotelServices = require('../services/FlightPlusHotelService');
let utilServices = require('../services/UtilService');

function* getDetail() {
    try {
		this.params.channel = this.authType;   
		this.params.customerIpAddress = ((this.request.header['x-forwarded-for'] || this.request.ip).replace(/::ffff:/g, ''));
		this.params.customerUserAgent = this.request.header['user-agent'];
		let params = getHotelDetailParams(this.params);
		this.body = yield flightAndHotelServices.getHotelDetail(params, this.authSession);
    } catch (err) {
		console.log("Error Catch", err);
		this.status = err.status || 500;
		this.body = err.message || err;
		// this.app.emit('error', err, this);
	}
}

function* setBlacklist() {
    try {
		this.body = yield flightAndHotelServices.setBlacklist();
    } catch (err) {
		console.log("Error Catch", err);
		this.status = err.status || 500;
		this.body = err.message || err;
	}	
}

function* setRateRules() {
    try {
		this.body = yield flightAndHotelServices.setRateRules();
    } catch (err) {
		console.log("Error Catch", err);
		this.status = err.status || 500;
		this.body = err.message || err;
	}	
}

function getHotelDetailParams(_params) {
	if(!_.has(_params,'searchId')){
		throw {
			 status:400
			,message:'missing search Identifier parameter'
		};
	}
	let parameters = {
		hotelID:  _params.hotel,
		arrivalDate:  _params.departure,
		departureDate: _params.returning,
		searchId: _params.searchId,
		//supplier:( env !== 'production' ? 'EAN' : ( env === 'qa' ? 'EANPKGR' : 'EAN,EANPKGR'))
		supplier: getSupplier(_params.supplier),
		country:'CL',
		channel:_params.channel || 'B2B',
		customerIpAddress : utilServices.getIp(_params.customerIpAddress),
		customerUserAgent : _params.customerUserAgent,
		//meta:(( env !== 'production' || _params.meta) ? true : false )		
		meta:true
	};
	if(_.has(_params,'latitude') && _.has(_params,'longitude')){
		parameters.latitude = _params.latitude;
		parameters.longitude = _params.longitude;
	}

	utilServices.addPaxAndRooms(_params, {}, parameters);
	return parameters;
}

function getSupplier(_supplier){
	if(!_supplier){
		return 'EAN';
	}
	switch(_supplier){
		case '1':
		return 'EAN';
		case '2':
		return 'EANPKGR';
		case '3':
		return 'TRV';
		default:
		return 'EAN';
	}	
}

module.exports = {
	getDetail: getDetail,
	setBlacklist: setBlacklist,
	setRateRules: setRateRules
};
