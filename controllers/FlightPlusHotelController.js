/**
 * FlightPlusHotelController.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let externalServices = require('cocha-external-services');
let crmServices = require('cocha-external-services').crmServices;
let flightAndHotelServices = require('../services/FlightPlusHotelService');
let utilServices = require('../services/UtilService');
let flightHotelServices = externalServices.flightHotelServices;
let hotelServices = externalServices.hotelServices;
let sendMailService = externalServices.sendMail;
let webServices = externalServices.webServices;
let paymentSessionServices = externalServices.paymentSession;
let mailParser = require('../services/MailingParser');
let bookingModel = require('../models/redis/Booking');
let oportunityModel = require('../models/redis/Oportunity');

function* autocomplete() {
	let params = {
		name: this.params.name
	};
	this.body = yield flightAndHotelServices.autocomplete(params);
}
function* get() {
	this.params.channel = this.authType;
	this.params.customerIpAddress = ((this.request.header['x-forwarded-for'] || this.request.ip).replace(/::ffff:/g, ''));
	this.params.customerUserAgent = this.request.header['user-agent'];	
	this.authSession.logLevel = 'error';
	let params = getFlightHotelParams(this.params);
	let result = yield flightAndHotelServices.getFlightsWithHotels(params, this.authSession);
	setCalendarFares(this.params, result, this.authSession);
	this.body = result;
}
function* pricing() {
	this.params.channel = this.authType;
	this.params.customerIpAddress = ((this.request.header['x-forwarded-for'] || this.request.ip).replace(/::ffff:/g, ''));	
	this.params.customerUserAgent = this.request.header['user-agent'];	
	let params = pricingFlightHotelParams(this.params, {});
	this.body = yield flightAndHotelServices.pricingFlightsWithHotels(params, this.authSession);
}
function* quote() {
	console.log(this.params, this.request.body);
	this.params.channel = this.authType;
	let params = pricingFlightHotelParams(this.params, this.request.body);
	this.body = yield flightAndHotelServices.quoteFlightsWithHotels(params, this.authSession);
}

function* booking() {
	// this.authType = 'B2B'; // Para forzar B2B	
	this.params.channel = this.authType;
	this.params.sellerUser = this.authSession.user;
	let bookData = yield new Promise((resolve, reject) => {
		flightHotelServices.booking(this.params, (err, result) => {
			if (err) {
					Koa.log.error(err);
					reject({
						message: {
							msg: err.msg || err,
							code: err.name
						}, 
						data: err
					});
			} else {			
				resolve(result);
			}
		}, this.authSession);
	});

	crmServices.flightHotelRegister({
		info: {
			contact: this.params.contact,
			passengers: this.params.passengers,
			spnr: bookData.spnr || null
		},
		register: this.params.receiveNews
	}, 
	function (err, registered) {
		if (err) {
			Koa.log.error("Error in subscription: " + JSON.stringify(err));
		} else {
			Koa.log.info("Success subscription: " + JSON.stringify(registered));
		}
	});

	if (this.params.paymentMethod === 'PagoDiferido'){
		try {
			oportunityModel.delPnrOportunity(this.params.contact.email); //Borramos para evitar el doble flujo

			let config = yield mailParser.parseSolicitudePayment(bookData);
			sendMailService.sendMail(config, (err, result) => {
				if (err) {
					Koa.log.error(err);
				}
			});
		} catch (err) {
			Koa.log.error(err);
		}
	} else
	if (this.params.channel === 'B2C' && bookData.spnr) {
		if (this.params.paymentMethod === 'ItauPuntos') {
			let paymentSessionData = yield setPaymentToCollector(this.params, bookData, this.authSession);
			bookData.token = paymentSessionData.paymentToken;
			bookData.url = paymentSessionData.url;
		}
		if (this.params.info) {
			oportunityModel.setOportunity(this.params.contact.email, this.params); //Dura 36h y no se porque
			
			oportunityModel.setPnrOportunity(this.params.contact.email, {pnr: bookData.spnr, email: this.params.contact.email});
		}
	}

	if (bookData.status === 'CompleteAsyncB2B') {
		let processStatus = {
			bookingFlight: true,			
			confirmingHotelAvailability: false,
			chargingPayment: false,
			bookingHotel: false,
			registeringInSmart: false
		};

		bookingModel.setBooking(bookData.spnr, {
			status: 'Process',
			progress: processStatus,
			bookingResponse: bookData
		});
		this.body = {
			status: 'CompleteAsyncB2B',
			checkobookUrl: '/checkbooking/' + bookData.spnr,
			checkobookStatus: processStatus
		};		
	} else {
		this.body = bookData;
	}
};

function* checkBooking() {
	// this.authType = 'B2B'; // Para forzar B2B		
	if (this.authType === 'B2B') {
		let booking;
		try {
			booking = yield bookingModel.getBooking(this.params.spnr);
		} catch(error){
			throw {
				status: 404,
				message: {
					msg: 'Book can not process',
					code: 'InternalError' 
				},
				data: error
			};
		}

		if (booking.status === 'Error') {
			throw {
				message: {
					msg: booking.error.msg,
					code: booking.error.code
				},
				data: booking.error.data
			};
		} else
		if (booking.status === 'Process') {			
			let steps = yield flightAndHotelServices.trackBooking(this.params.spnr);

			if (steps.error){
				booking.status = 'Error';
				booking.error = steps.error;
				bookingModel.setBooking(this.params.spnr, booking);
				throw {
					message: {
						msg: steps.error.msg,
						code: steps.error.code
					},
					data: steps.error.data
				};
			}

			if (steps.reponse) {
				booking.bookingResponse.status = steps.reponse.status;
				booking.bookingResponse.authPrice = steps.reponse.authPrice;
			}

			steps = steps.progress;

			let newBooking = _.cloneDeep(booking);
			newBooking.progress.bookingFlight = (steps.availabilityFlight && steps.bookFlight && steps.bookStep);
			newBooking.progress.confirmingHotelAvailability = steps.availabilityHotel;
			newBooking.progress.chargingPayment = (steps.getBusinessOne && steps.payOnHost);
			newBooking.progress.bookingHotel = (steps.bookHotel && steps.sendMail);
			newBooking.progress.registeringInSmart = (steps.getBusinessTwo && steps.sendSmartFlight && steps.sendSmartHotel);

			let resp;
			if (steps.bookEnd) {
				newBooking.status = 'Ended';
				resp = {
					checkobookStatus: newBooking.progress,
					bookingResponse: booking.bookingResponse
				}
			} else {
				newBooking.status = 'Process';
				resp = {
					checkobookStatus: newBooking.progress
				}
			}

			if (JSON.stringify(newBooking) !== JSON.stringify(booking)) {
				bookingModel.setBooking(this.params.spnr, newBooking);
			}

			this.body = resp;
		} else
		if (booking.status === 'Ended') {
			this.body = {
				checkobookStatus: booking.progress,
				bookingResponse: booking.bookingResponse
			};
		}
	} else {
		throw {
			status: 401,
			message: {
				msg: 'Access denied',
				code: 'AccessError'
			}
		};
	}
}

function* checkPay() {
	this.body = yield new Promise((resolve, reject) => {
		flightHotelServices.checkPayment(this.params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject({
					message: {
						msg: err.msg || err,
						code: err.name
					},
					data: err
				});
			} else {
				resolve(result);
			}
		}, this.authSession);
	});
}

function* sendPaymentOportunity() {

	var emailTo = this.params.emailTo.toUpperCase() || null;
	var emailBcc = "agenteinternet@cocha.com";

	var config = yield mailParser.parsePaymentOportunity(emailTo, emailBcc);

	if (config) {
		var resp = yield new Promise((resolve, reject) => {
			sendMailService.sendMail(config, (err, result) => {
				if (err) {
					Koa.log.error(err);
					reject(err);
				} else {
					resolve(result);
				}
			});
		});
		this.body = { msg: resp };
	} else {
		this.body = { msg: "Email already sent" };
	}
};

function* checkCreditLine() {
	if (this.authType === 'B2B') {
		this.body = yield new Promise((resolve, reject) => {
			flightHotelServices.getCreditLineInfo(this.authSession.user, (err, result) => {
				if (err) {
					Koa.log.error(err);
					reject({
						message: {
							msg: err.msg || err,
							code: err.name
						},
						data: err
					});
				} else {
					resolve(result);
				}
			}, this.authSession);
		});
	} else {
		throw {
			status: 401,
			message: {
				msg: 'Access denied',
				code: 'AccessError'
			}
		};
	}
}

function* sendItinerary() {
	this.params.channel = this.authType;
	var email = this.params.email || null;
	// this.params.channel = 'B2B'; // Para forzar B2B
	let itinerary = yield new Promise((resolve, reject) => {
		flightHotelServices.itinerary(this.params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject({
					message: {
						msg: err.msg,
						code: err.name
					}
				});
			} else {
				resolve(result);
			}
		}, this.authSession);
	});

	var config = yield mailParser.parseConfirmationFlightHotel(itinerary, email);
	this.body = yield new Promise((resolve, reject) => {
		sendMailService.sendMail(config, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
};

function* sendItineraryWithIds() {
	this.params.channel = this.authType;
	var email = this.params.email || null;
	var lastname = this.params.lastname || null;
	// this.params.channel = 'B2B'; // Para forzar B2B
	let itinerary = yield new Promise((resolve, reject) => {
		flightHotelServices.itineraryWithIds(this.params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject({
					message: {
						msg: err.msg,
						code: err.name
					}
				});
			} else {
				resolve(result);
			}
		}, this.authSession);
	});

	var config = yield mailParser.parseConfirmationFlightHotelWithIds(itinerary, email, lastname);
	this.body = yield new Promise((resolve, reject) => {
		sendMailService.sendMail(config, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
};

function* sendDeferredPayment() {
	this.params.channel = this.authType;
	var email = this.params.email || null;
	// this.params.channel = 'B2B'; // Para forzar B2B
	let itinerary = yield new Promise((resolve, reject) => {
		flightHotelServices.itinerary(this.params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject({
					message: {
						msg: err.msg,
						code: err.name
					}
				});
			} else {
				resolve(result);
			}
		}, this.authSession);
	});

	var config = yield mailParser.parseDeferredPayment(itinerary, this.params, email);

	this.body = yield new Promise((resolve, reject) => {
		sendMailService.sendMail(config, (err, result) => {
			if (err) {
				Koa.log.error(err);		
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
};

function* getPdfVoucher() {
	this.params.channel = this.authType;

	let itinerary = yield new Promise((resolve, reject) => {
		flightHotelServices.itinerary(this.params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject({
					message: {
						msg: err.msg,
						code: err.name
					}
				});
			} else {
				resolve(result);
			}
		}, this.authSession);
	});

	var pdfStream = yield mailParser.parsePdfVoucher(itinerary);

	this.body = pdfStream;
	this.response.set('Content-Type', 'application/pdf; charset=utf-8');
	this.attachment(this.params.spnr + '.pdf')

	return true;
};

function* callWait() {
	this.body = yield flightAndHotelServices.waitRequest(this.params.minutes || '1');
}

module.exports = {
	autocomplete: autocomplete,
	get: get,
	pricing: pricing,
	quote: quote,
	booking: booking,
	checkBooking: checkBooking,
	checkPay: checkPay,
	sendItinerary: sendItinerary,
	getPdfVoucher: getPdfVoucher,
	sendPaymentOportunity: sendPaymentOportunity,
	sendDeferredPayment: sendDeferredPayment,
	checkCreditLine: checkCreditLine,
	sendItineraryWithIds: sendItineraryWithIds,
	callWait: callWait
};

function getFlightHotelParams(_params) {
	let flightParameters = {
		origin: _params.origin,
		destination: _params.destination,
		departureDate: _params.departure,
		returnDate: _params.returning,
		cabin: _params.cabin || 'Y',
		airlines: _params.airlines || '',
		stops: _params.stops || 3,
		// specificArrival: (_params.checkin && _params.checkout) ? null : _params.departure,
		product: 'V+H',
		channel: 'B2C',
		parsePrice: true,
		addFee: (_params.channel === 'B2B')
	};

	let baseHotelParams = {
		arrivalDate: _params.checkin || _params.departure,
		departureDate: _params.checkout || _params.returning,
		destinationID: _params.region,
		supplier: (env === 'production' ? 'EAN,EANPKGR' : 'EAN,EANPKGR,TRV'),
		start: _params.start || 0,
		limit: _params.limit || 9999,
		channel: _params.channel,
		customerIpAddress : utilServices.getIp(_params.customerIpAddress),
		customerUserAgent : _params.customerUserAgent,
		//meta:(( env !== 'production' || _params.meta) ? true : false )						
		meta:true
	};

	utilServices.addPaxAndRooms(_params, flightParameters, baseHotelParams);

	let hotelParameters = {
		just: _.cloneDeep(baseHotelParams),
		late: _.cloneDeep(baseHotelParams)
	};

	if (_params.checkin && _params.checkout) {
		hotelParameters.late = false;
	} else {
		hotelParameters.late.arrivalDate = moment(hotelParameters.late.arrivalDate, "YYYY-MM-DD").add(1, 'days').format("YYYY-MM-DD");
	}

	let transferParameters = {
		ianCode: _params.region,
		iataCode: _params.destination,
		channel: _params.channel
	};

	return {
		hotels: hotelParameters,
		flights: flightParameters,
		transfers: transferParameters
	};
}

function pricingFlightHotelParams(_params, _body) {
	let flightParameters = {
		departureCode: _params.departurecode,
		returnCode: _params.returncode || null,
		product: 'V+H',
		channel: _params.channel
	};

	let hotelParameters = {
		hotelID: _params.hotelID,
		roomTypeId: _params.roomTypeCode,
		rateCode: _params.rateCode,
		roomTypeCode: _params.roomTypeCode,
		arrivalDate: _params.departure,
		departureDate: _params.returning,
		supplier: _params.supplier,
		channel: _params.channel,
		rating: _body.rating || 0,
		tripAdvisor: _body.tripAdvisor || 0,
		pages: _params.pages || true,
		discount: _body.discount || 0,
		recommended: _body.preferredHotel || false,
		image: _body.hotelImage,
		roomImages: _body.roomImages,
		customerIpAddress : utilServices.getIp(_params.customerIpAddress),
		customerUserAgent : _params.customerUserAgent,
		//meta:(( env !== 'production' || _params.meta) ? true : false )		
		meta:true
	};

	utilServices.addPaxAndRooms(_params, flightParameters, hotelParameters);

	let transferParameters = {
		ianCode: _params.region,
		iataCode: _params.destination,
		channel: _params.channel
	};

	return {
		hotels: hotelParameters,
		flights: flightParameters,
		transfers: transferParameters,
		transfer: _params.transfer,
		assistance: _params.assistance,
		quote: _params.quote || null,
		quoteEnv: _body.quoteEnv || 'www',
		price: Number(_body.price),
		deeplink: _body.deeplink || null
	};
}

function setCalendarFares(_params, _flightHotelList, _workflowId) {
	let url = Koa.config.path.setCalendarFares;	
	
	let data = {
		id: _params.departure + '-' + 
		_params.origin + '-' +
		_params.destination + '-' + 
		_params.region +  
		(_params.cabin || 'Y') + '-' + 
		(Math.round(moment(_params.returning).diff(moment(_params.departure), 'days',true)) + 1),
		fares: _flightHotelList
	};
	webServices.post('flightshotels', url, data, null, (err, result) => {
		if (err) {
			Koa.log.error(err);
		} else {
			Koa.log.info(result);
		}
	}, _workflowId);
}

function setPaymentToCollector(_params, _bookData, _workflowId) {
	let url = Koa.config.path.setPaymentToCollector;
	let data = {  
		paymentSrc: 'VyH',
		ccode: _bookData.xpnr,
		title: 'V+H: ' + _params.destination,
		clpPrice: Number(_bookData.authPrice),
		origin: _params.origin,
		destination: _params.destination,
		departureDate: _params.arrival,
		returningDate: _params.departure,
		contactEmail: _params.contact.email,
		totalRooms: _params.rooms.length,
		adult: 0,
		child: 0,
		infant: 0,
		wappOkUrl: Koa.config.path.collectorWappOkReturn,
		wappErrorUrl: Koa.config.path.collectorWappOkReturn
	};
	_.each(_params.passengers, function(passenger){
		if (passenger.type === 'ADT') {
			data.adult++;
		} else
		if (passenger.type === 'CHD') {
			data.child++;
		} else
		if (passenger.type === 'INF') {
			data.infant++;
		}
	});

	return new Promise((resolve, reject) => {
		webServices.post('flightshotels', url, data, null, (err, result) => {
			if (err) {
				reject({
					message: {
						msg: err.msg || err,
						code: err.name || err.data.name
					}, 
					data: err
				});
			} else {
				resolve(result);
			}
		}, _workflowId);
	});
}