'use strict';
/* jshint strict: false, esversion: 6 */

module.exports = {
	checkOpportunityPostBooking: {
		time: '*/20 * * * *',
		service: 'FlightPlusHotelService',
		action: 'checkOpportunity'
	}
};