'use strict';
/* jshint strict: false, esversion: 6 */

let os = require('os');
let redisService = require('./redisDatasource');

function* checkSentinel(sentinelId) {
  return new Promise((resolve, reject) => { 
  //return yield new Promise((resolve, reject) => { //sonar
    redisService.existInRedis(sentinelId, (err, exist) => {
      if (err) {
        resolve(false);
      } else {
        var hotsName = os.hostname();
        if (exist) {
          validateSentinel(sentinelId, hotsName, (rest) => {resolve(rest);});
        } else {
          redisService.setToRedis(sentinelId, hotsName, 900);
          setTimeout(() => {
            validateSentinel(sentinelId, hotsName, (rest) => {resolve(rest);});
          }, 2000);
        }
      }
    });
  });
}

module.exports = {
	checkSentinel: checkSentinel
};

function validateSentinel(sentinelId, hotsName, cb) {
	redisService.getFromRedis(sentinelId, function(err, sentinel) {
		if (err) {
			cb(false);
		} else
		if (hotsName === sentinel) {
			cb(true);
			redisService.delFromRedis(sentinelId, function(err, deleted) {
				if (deleted) {
					Koa.log.debug('Sentinel: ' + sentinelId + ' => Borrado exitoso');
				} else {
					Koa.log.error('Sentinel: ' + sentinelId + ' => Borrado no exitoso');
				}
			});
		} else {
      cb(false);
    }
	});
}