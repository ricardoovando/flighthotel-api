'use strict';
/* jshint strict: false, esversion: 6 */

let session = require('cocha-external-services').session;

// function isB2CForbidden(_params,_route){
//    return ((_route.action === 'booking' && _params.payMode === 'HostToHost') ? true: false);
// }

module.exports = {
	tokenStrategy: function* (_headers, _params) {
		if (_.has(_headers, 'sid')) {
			return new Promise((resolve, reject) => {  
			//return yield new Promise((resolve, reject) => { //sonar
				session.get({token: _headers.sid}, (err, result) => {
					if (err) {
						resolve(false);
					} else {
						session.setTTL({token: _headers.sid}, (err, result) => {});
						result.sidKey = _headers.sid;
						result.trackId = _headers.trackid;
						result.flowId = _headers.flowid;
						resolve({
							type: 'B2B',
							sessionData: result
						});
					}
				});
			});
		} else {
			return yield {
				type: 'B2C',
				sessionData: {
					trackId: _headers.trackid,
					flowId: _headers.flowid
				}
			};
		}
		// else { //b2c
		//   if(isB2CForbidden(_params,_route)){
		//   	return false;
		//   } else {
		//     return true;
		//   }
		// }
	}
};