'use strict';
/* jshint strict: false, esversion: 6 */

module.exports = {
	'/autocomplete/:name': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'autocomplete',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/search/:origin/:destination/:region/:departure/:returning': { //?room1=a,x,x,x&roomX=a,x,x,x&cabin=M&stops=0&airlines=LA,CM&checkin=YYYY-MM-DD&checkout=YYYY-MM-DD
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'get',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/pricing/:origin/:destination/:region/:departurecode/:returncode/:rateCode/:roomTypeCode/:hotelID/:supplier/:departure/:returning': { //?room1=a,x,x,x&roomX=a,x,x,x&transfer=aaaa&assistance=x
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'pricing',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/quote/:origin/:destination/:region/:departurecode/:returncode/:rateCode/:roomTypeCode/:hotelID/:supplier/:departure/:returning/:quote': { //?room1=a,x,x,x&roomX=a,x,x,x&transfer=aaaa&assistance=x&price=xxxx&rating=x&quote_env
		method: 'POST',
		controller: 'FlightPlusHotelController',
		action: 'quote',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/booking': {
		method: 'POST',
		controller: 'FlightPlusHotelController',
		action: 'booking',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/checkbooking/:spnr': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'checkBooking',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/checkpay/:token': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'checkPay',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/itinerary/sendDeferredPayment/:spnr/:amount/:expireDateTime/:transfer/:assistance': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'sendDeferredPayment',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/itinerary/sendDeferredPayment/:spnr/:amount/:expireDateTime/:transfer/:assistance/:email': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'sendDeferredPayment',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/itinerary/send/:spnr': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'sendItinerary',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/itinerary/send/:spnr/:email': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'sendItinerary',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/itineraryWithIds/send/:epnr/:itIDHotel/:lastname': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'sendItineraryWithIds',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/itineraryWithIds/send/:epnr/:itIDHotel/:lastname/:email': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'sendItineraryWithIds',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/itinerary/status/:id': {
		method: 'GET',
		controller: 'MailingController',
		action: 'getSentInfo',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/itinerary/sendOportunity/:emailTo': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'sendPaymentOportunity',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/itinerary/pdf/:spnr': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'getPdfVoucher',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/hotel/:hotel/:departure/:returning': { // 'GET /hotel/:hotel/:departure/:returning': 'HotelDetailController.get' //?room1=a,x,x,x&roomX=a,x,x,x
		method: 'GET',
		controller: 'HotelController',
		action: 'getDetail',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/blacklist': { // 'GET /blacklist': 'HotelController.setBlacklist' 
		method: 'GET',
		controller: 'HotelController',
		action: 'setBlacklist',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/rateRules': { // 'GET /rateRules': 'HotelController.setRateRules' 
		method: 'GET',
		controller: 'HotelController',
		action: 'setRateRules',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/creditline': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'checkCreditLine',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	},
	'/waiter-proxy/:minutes': {
		method: 'GET',
		controller: 'FlightPlusHotelController',
		action: 'callWait',
		auth: {
			strategy: 'tokenStrategy',
			redirect: null,
		}
	}
};