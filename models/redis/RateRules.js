/**
 * RateRules.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let RedisService = require('../../config/redisDatasource');

function setRateRules(list) {
	RedisService.setToRedis("rateRules",list,-1);
}

function getRateRules() {
	return new Promise((resolve, reject) => {
		RedisService.getFromRedis("rateRules", (err, result) => {
			if (err) {
				resolve({});
			} else {
				resolve(result);
			}
		});
	});
}

module.exports = {
	setRateRules: setRateRules,
	getRateRules: getRateRules
};