/**
 * Delta.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let RedisService = require('../../config/redisDatasource');

function setDelta(deltaId, delta) {
	RedisService.setToRedis("delta:" + deltaId, delta, Koa.config.redisConf.expireDelta);
}

function getDelta(deltaId) {
	return new Promise((resolve, reject) => {
		RedisService.getFromRedis("delta:" + deltaId, (err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
}

module.exports = {
	setDelta: setDelta,
	getDelta: getDelta
};