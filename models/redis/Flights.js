/**
 * Flights.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let RedisService = require('../../config/redisDatasource');

function setFlight(flightId, flight) {
	RedisService.setToRedis("flights:" + flightId, flight);
}

function getFlight(flightId) {
	return new Promise((resolve, reject) => {
		RedisService.getFromRedis("flights:" + flightId, (err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
}

module.exports = {
	setFlight: setFlight,
	getFlight: getFlight
};