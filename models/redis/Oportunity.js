/**
 * Oportunitys.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let RedisService = require('../../config/redisDatasource');

function setOportunity(OportunityId, Oportunity) {
	RedisService.setToRedis("Oportunities:" + OportunityId, Oportunity, Koa.config.redisConf.expireOportunity);
}

function setPnrOportunity(OportunityId, PnrOportunity) {
	RedisService.setToRedis("PnrOportunity:" + OportunityId, PnrOportunity, 3600);
}

function getOportunity(OportunityId) {
	return new Promise((resolve, reject) => {
		RedisService.getFromRedis("Oportunities:" + OportunityId, (err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
}

function getPnrOportunities() {
	return new Promise((resolve, reject) => {
		RedisService.getFromRedisWithPattern("PnrOportunity:*", (err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result.reply);
				RedisService.delFromRedis(result.keys, (err, result) => {
					if (err) {
						Koa.log.error('Error deleting from Redis. Result: ' + err);
					}
				});
			}
		});
	});
}

function existsOportunity(OportunityId) {
	return new Promise((resolve, reject) => {
		RedisService.existInRedis("Oportunities:" + OportunityId, (err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
}

function delPnrOportunity(OportunityId) {
	return new Promise((resolve, reject) => {
		RedisService.delFromRedis("PnrOportunity:" + OportunityId, (err, result) => {
			if (err) {
				Koa.log.error('Error deleting from Redis. Result: ' + err);
			} else {
				resolve(result);
			}
		});
	});
}

module.exports = {
	setOportunity: setOportunity,
	setPnrOportunity: setPnrOportunity,
	getOportunity: getOportunity,
	getPnrOportunities: getPnrOportunities,
	existsOportunity: existsOportunity,
	delPnrOportunity: delPnrOportunity
};
