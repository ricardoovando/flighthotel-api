'use strict';
/*jshint esversion: 6 */

const PORT = process.env.PORT || 1337;
const ENV = process.env.NODE_ENV || 'production';

global.Koa = {};
global.Koa.config = require('./config/environment/' + ENV);
global.Koa.log = require('./config/logger');

let authenticator = require('./config/authenticator');
let routes = require('./config/routes');
let crons = require('./config/crons');
let utils = require('./config/utils');

global.env = ENV;
global._ = require('lodash');
global.moment = require('moment');

let app = require('koa')();
let router = require('koa-router')();
let bodyParser = require('koa-bodyparser')();

let cronJob = require('cron').CronJob;
let crypto = require('crypto');
let generatorControl = require('co');

let redisService = require('./config/redisDatasource');
let mysqlService = require('./config/mysqlDatasource');
mysqlService.start(); //Init MySQL database access // Buscar una major manera

if (Koa.config.crons.enabled) {
	_.forEach(crons, (cron, key) => {
		let action = (typeof cron.action === 'function')? cron.action : require('./services/' + cron.service)[cron.action];
		let job = new cronJob({
			cronTime: cron.time,
			onTick: function() {
				generatorControl.wrap(function* () {
					if (yield utils.checkSentinel(key)) {
						yield action();
					}
				}).call({})
				.then(() => {
					Koa.log.debug('Cron Job: ' + key + ' => End');
				})
				.catch((error) => {
					Koa.log.error('Cron Job: ' + key + ' => Error: ', error);
				});
			},
			start: false
		});
		job.start();
	});
}

_.forEach(routes, (route, key) => {
	router[route.method.toLowerCase()](key, function* (next) {
		let asyncode = this.request.header.req;

		let valid = true;
		if (route.auth) {
			valid = yield authenticator[route.auth.strategy](this.request.header, this.params);
			this.authType = (valid.type)? valid.type : valid;
			this.authSession = (valid.sessionData)? valid.sessionData : this.authSession;
		}

		if (valid) {
			let executable = require('./controllers/' + route.controller)[route.action];
			if (asyncode) {
				if (asyncode === 'async') {
					let context = {
						params: _.cloneDeep(this.params),
						authType: _.cloneDeep(this.authType),
						authSession: _.cloneDeep(this.authSession),
						status: undefined,
						body: this.body
					};
					let reqCode =  crypto.randomBytes(16).toString("hex");

					setTimeout(() => {
						let flowControl = generatorControl.wrap(executable);
						flowControl.call(context)
							.then(() => {
								let result = {
									type: 'COMPLETE',
									status: context.status || 200,
									data: context.body
								};
								redisService.setToRedis("asynCall:" + reqCode, result, Koa.config.redisConf.expireAsync);
							})
							.catch((error) => {
								let result = {
									type: 'ERROR',
									status: error.status,
									data: error
								};
								redisService.setToRedis("asynCall:" + reqCode, result, Koa.config.redisConf.expireAsync);
							});
					}, 100);

					let result = {
						type: 'PENDING',
						status: 202,
						data: {
							msg: "pending",
							id: reqCode
						}
					};
					redisService.setToRedis("asynCall:" + reqCode, result, Koa.config.redisConf.expireAsync);

					this.status = result.status;
					this.body = result.data;
				} else {
					let result = yield new Promise((resolve, reject) => {
						redisService.getFromRedis("asynCall:" + asyncode, (err, result) => {
							if (err) {
								Koa.log.error(err);
								reject({status: 404, message: 'Async code not found'});
							} else {
								resolve(result);
							}
						});
					});

					if (result.type === 'ERROR') {
						throw result.data;
					} else {
						this.status = result.status;
						this.body = result.data;
					}
				}
			} else {
				yield executable;
			}
			// yield require('./controllers/' + route.controller)[route.action];
		} else {
			if (route.auth.redirect) {
				this.redirect(route.auth.redirect);
			} else {
				throw {status: 401, message: 'Access denied'};
			}
		}
	});
});

app.use(bodyParser);

app.use(function* (next){
	this.params = _.assign(this.params, this.request.query, this.request.body);
	this.authSession = {};

  this.req.socket.setTimeout(900000);

/*
	Manejo de respuestas en error (Por hacer)
	status = 404;
	caller = 'xxxx'
	code = '003';
	message = 'La información solicitada no está disponible.';
*/
	try {
		yield next;
	} catch (err) {
		Koa.log.error(err);
		let message = err.message || err;
		let status = err.status || 500;

		Koa.log.errorSL({
			action: this.request.url,
			searchParams: this.params,
			status: status,
			message: message,
			data: err.data || err
		}, this.authSession);

		this.status = status;
		this.body = (_.isString(message))? {msg: message, code: 'UnknowError'} : message;
	}

	this.response.remove('Connection');
	this.response.set('Access-Control-Allow-Origin', '*');
	this.response.set('Access-Control-Allow-Headers', 'sid, TrackId, FlowId, Origin, X-Requested-With, Content-Type, Accept, Authorization');
	this.response.set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	this.response.set('Content-Type', 'application/json; charset=utf-8');
});

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(PORT, () => {
	Koa.log.info('Server running at');
	Koa.log.info('PORT: ' + PORT);
	Koa.log.info('ENV: ' + ENV);
});
