/**
 * FlightPlusHotelParser.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

var HotelParser             = require('./HotelParser');
var rateRulesModel			= require('../models/redis/RateRules');

function* parseFlightsPlusHotels(_parameters, _results) {
	var _hotelIndex = {};	
	var flightTypeIndex = {};

	var justIndex  = "just";	  
	var lateIndex  = "late";

	let deltaId = _results.flights.referencePrice.id;

	for(var i=0;i<_results.flights.found.length;i++) {
		for(var j=0;j<_results.flights.found[i].departures.length;j++) {
			flightTypeIndex[_results.flights.found[i].departures[j].arrivalType] = true;
		}
	}

	console.log('fjust',flightTypeIndex[justIndex]);
	console.log('flate',flightTypeIndex[lateIndex]);	
	console.log('hjust',_results.hotelsJust.found.length);
	console.log('hlate', (_results.hotelsLate.found)? _results.hotelsLate.found.length : 0);

	if(!_results.hotelsLate) { //checkin checkout case
		if ((!flightTypeIndex[justIndex] && !flightTypeIndex[lateIndex]) ||  (_results.hotelsJust.found.length === 0)) {
			throw {
				status: 404,
				message: 'No se encontraron resultados'
			};
		}
	} else { //normal search
		//console.log("VUELOS",_results.flights.found.length);

		if ((!flightTypeIndex[justIndex] && !flightTypeIndex[lateIndex]) ||  (_results.hotelsJust.found.length === 0 && _results.hotelsLate.found.length === 0)) {
			throw {
				status: 404,
				message: 'No se encontraron resultados'
			};
		}
		for(var k=_results.flights.found.length-1;k>=0;k--) {
			for(var l=_results.flights.found[k].departures.length-1;l>=0;l--) {
				if(!_results.hotelsJust.found.length && _results.flights.found[k].departures[l].arrivalType === justIndex) {
					_results.flights.found[k].departures.splice(l,1);
				} else if(!_results.hotelsLate.found.length && _results.flights.found[k].departures[l].arrivalType === lateIndex) {
					_results.flights.found[k].departures.splice(l,1);
				}
			}
			if(!_results.flights.found[k].departures.length) {
				_results.flights.found.splice(k,1);
			}
		}
	}

	for (var m = 0; m < _results.hotelsJust.found.length; m++) {
		_results.hotelsJust.found[m].price = {};
		_results.hotelsJust.found[m].price[justIndex] = {
			usd: getRates(_results.hotelsJust.found[m].rate, _results.flights.referencePrice.usd, _results.hotelsJust.exchangeRate),
			clp: getRates(_results.hotelsJust.found[m].rate, _results.flights.referencePrice.clp, _results.hotelsJust.exchangeRate)
		};
		//console.log("just",_results.hotelsJust.found[i].price[justIndex].usd.supplier,'-',_results.hotelsJust.found[i].hotelId);
		_hotelIndex[_results.hotelsJust.found[m].hotelId] = m;
		delete _results.hotelsJust.found[m].rate;
	}

	if (_results.hotelsLate) {
		//var rateRules      = yield rateRulesModel.getRateRules();
		//var rateParameters = HotelParser.getRateParameters(_parameters.hotels.late);
		for (var n = 0; n < _results.hotelsLate.found.length; n++) {
			//console.log(_hotelIndex[_results.hotelsLate.found[n].hotelId],_results.hotelsLate.found[n].hotelId);
			if(_hotelIndex[_results.hotelsLate.found[n].hotelId] >= 0) {
				//_results.hotelsLate.found[n].rate = HotelParser.getRates(_results.hotelsLate.found[n].rateInfo.rateForSupplier, rateParameters, rateRules[_results.hotelsLate.found[i].hotelId]);
				_results.hotelsJust.found[_hotelIndex[_results.hotelsLate.found[n].hotelId]].price[lateIndex] =  {
					usd: getRates(_results.hotelsLate.found[n].rate, _results.flights.referencePrice.usd, _results.hotelsJust.exchangeRate),
					clp: getRates(_results.hotelsLate.found[n].rate, _results.flights.referencePrice.clp, _results.hotelsJust.exchangeRate)
				};
				//console.log("late",_results.hotelsJust.found[_hotelIndex[_results.hotelsLate.found[i].hotelId]].price[lateIndex].usd.supplier,'-',_results.hotelsLate.found[i].hotelI);
			} else {
				let newHotel = _results.hotelsLate.found[n];
				newHotel.price = {};
				newHotel.price[lateIndex] = {
					usd: getRates(_results.hotelsLate.found[n].rate, _results.flights.referencePrice.usd, _results.hotelsJust.exchangeRate),
					clp: getRates(_results.hotelsLate.found[n].rate, _results.flights.referencePrice.clp, _results.hotelsJust.exchangeRate)
				};
				_results.hotelsJust.found.push(newHotel);
			}
		}
	} 

	_results.hotelsJust.total = _results.hotelsJust.found.length;

	delete _results.flights.referencePrice;

	let exchangeRate = _results.hotelsJust.exchangeRate;
	delete _results.hotelsJust.exchangeRate;
	delete _results.flights.exchangeRate;

	let national = _results.flights.national;
	delete _results.flights.national;

	let totalPax = _parameters.flights.numADT + _parameters.flights.numCHD + _parameters.flights.numINF;

	return yield {
		flights: _results.flights,
		hotels: _results.hotelsJust,
		transfer: parseTransfer(_results.transfer, exchangeRate, totalPax),
		national: national,
		exchangeRate: exchangeRate,
		id: deltaId
	};
}

function* parsePricingFlightsPlusHotels(_parameters, _pricingFlight, _pricingHotel, _transfer, _hotelDetails) {
	delete _pricingFlight.departure.ref;
	_.each(_pricingFlight.departure.connections, function(connection) {
		delete connection.extraParamsList;
	});
	if (_pricingFlight.returning) {
		delete _pricingFlight.returning.ref;
		_.each(_pricingFlight.returning.connections, function(connection) {
			delete connection.extraParamsList;
		});	
	}

	let passengerConfig = {
		foid: _pricingFlight.departure.foid || false,
		docs: _pricingFlight.departure.docs || false
	};
	let exchangeRate = _pricingHotel.rate;
	let prices = getPricing(_pricingFlight, _pricingHotel, exchangeRate, _parameters);

	let rawPrices = {
		priceBase: {
			usd: {
				baseFlight: _pricingFlight.prices.usd.summary.base,
				taxesFlight: _pricingFlight.prices.usd.summary.taxes,
				feeFlight: _pricingFlight.prices.usd.summary.fee,
				feeB2bFlight: _pricingFlight.prices.usd.summary.feeB2b,
				totalFlight: _pricingFlight.prices.usd.summary.total,
				baseHotel: _pricingHotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.nightlyRateTotal,
				taxesHotel: _pricingHotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.surchargeTotal,
				totalHotel: _pricingHotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.total
			},
			clp: {
				baseFlight: _pricingFlight.prices.clp.summary.base,
				taxesFlight: _pricingFlight.prices.clp.summary.taxes,
				feeFlight: _pricingFlight.prices.clp.summary.fee,
				feeB2bFlight: _pricingFlight.prices.clp.summary.feeB2b,
				totalFlight: _pricingFlight.prices.clp.summary.total,
				baseHotel: _pricingHotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.nightlyRateTotalCLP,
				taxesHotel: _pricingHotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.surchargeTotalCLP,		
				totalHotel: _pricingHotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.totalCLP
			}
		},
		surcharges: _pricingHotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.surcharges,
		priceRefactor: prices,
		exchangeRate: exchangeRate
	};

	delete _pricingFlight.prices;

	let totalPax = _parameters.flights.numADT + _parameters.flights.numCHD + _parameters.flights.numINF;

	return yield {
		flightInfo: _pricingFlight,
		hotelInfo: parseHotelPricing(_pricingHotel, _hotelDetails, _parameters),
		transferInfo: parseTransfer(_transfer, exchangeRate, totalPax),
		national: _pricingFlight.departure.national,
		exchangeRate: exchangeRate,
		passengerConfig: passengerConfig,
		prices: prices,
		dataPrices: rawPrices
	};
}

function* parseQuotation(_parameters, _data) {
	let prices = _.cloneDeep(_data.prices);
	let assistanceIt = false;
	let tranferIt =  false;
	prices.usd.assistency = 0;
	prices.clp.assistency = 0;
	prices.usd.transfer = 0;
	prices.clp.transfer = 0;
	prices.usd.discount = _parameters.hotels.discount;

	if (_parameters.assistance && _parameters.assistance !== 'no') {
		let totalPax = _parameters.flights.numADT + _parameters.flights.numCHD + _parameters.flights.numINF;
		let travelDays = Math.round(moment(_data.flightInfo.returning.arrival).hour(0).minute(0).diff(moment(_data.flightInfo.departure.departure).hour(0).minute(0), 'd',true)) + 1;
		let assistanceUsdTotal = ((_data.national) ? 0 : 5) * travelDays * totalPax;
		let assistanceClpTotal = ((_data.national) ? 0 : (5 * _data.exchangeRate)) * travelDays * totalPax;

		prices.usd.assistency = Number(Math.ceil(assistanceUsdTotal).toFixed(0));
		prices.usd.total += assistanceUsdTotal;
		prices.clp.assistency = assistanceClpTotal;
		prices.clp.total += assistanceClpTotal;
		assistanceIt = true;
	}
	if (_parameters.transfer && _parameters.transfer !== 'no') {
		prices.usd.transfer = Number(Math.ceil(_data.transferInfo.prices.usd.total).toFixed(0));
		prices.usd.total += _data.transferInfo.prices.usd.total;
		prices.clp.transfer = _data.transferInfo.prices.clp.total;
		prices.clp.total += _data.transferInfo.prices.clp.total;
		tranferIt = true;
	}

	prices.usd.total += prices.usd.localTax;
	prices.clp.total += prices.clp.localTax;

	prices.usd.base = Number(Math.ceil(prices.usd.base).toFixed(0));
	prices.usd.taxes = Number(Math.ceil(prices.usd.taxes).toFixed(0));
	prices.usd.fee = Number(Math.ceil(prices.usd.fee).toFixed(0));
	prices.usd.feeB2b = Number(Math.ceil(prices.usd.feeB2b).toFixed(0));
	prices.usd.total = Number(Math.ceil(prices.usd.total).toFixed(0));
	prices.usd.localTax = Number(Math.ceil(prices.usd.localTax).toFixed(0));


	return yield {
		quote: _parameters.quote,
		domain: _parameters.quoteEnv,
		hotel: {
			name: _data.hotelInfo.name,
			location: _data.hotelInfo.address,
			image: _parameters.hotels.image || null,
			stars: _parameters.hotels.rating,
			checkin: _data.hotelInfo.checkin,
			checkout: _data.hotelInfo.checkout,
			nights: Math.round(moment(_data.hotelInfo.checkout).diff(moment(_data.hotelInfo.checkin), 'd',true)),
			includes: _data.hotelInfo.amenities,
			hotelAmenities: _data.hotelInfo.hotelAmenities,
			supplier: _parameters.hotels.supplier,
			instructions: _data.hotelInfo.checkInInstructions,
			checkInTime: _data.hotelInfo.checkInTime,
			checkOutTime: _data.hotelInfo.checkOutTime,
			rooms: parseRoomsQuote(_parameters.hotels),
			tripAdvisor: _data.hotelInfo.tripAdvisorRating || _parameters.hotels.tripAdvisor,
			tripAdvisorUrl: _data.hotelInfo.tripAdvisorUrl,
			tripAdvisorReviewCount: _data.hotelInfo.tripAdvisorReviewCount,
			occupancyPerRoom: _data.hotelInfo.rateOccupancyPerRoom,
			coordinates:_data.hotelInfo.coordinates,
			roomDescription: _data.hotelInfo.room.roomTypeDescription,
			bedTypes: _data.hotelInfo.room.bedTypes.bedType,
			refundable: !_data.hotelInfo.room.rateInfos.rateInfo.nonRefundable,
			recommended: _parameters.hotels.recommended,
			roomImages: _parameters.hotels.roomImages
		},
		flight: {
			going: {
				origin: parsePlace(_data.flightInfo.departure.origin,_data.flightInfo.places),
				destination: parsePlace(_data.flightInfo.departure.destination,_data.flightInfo.places),
				airline: parseAirline(_data.flightInfo.departure.airline,_data.flightInfo.airlines),
				image: getAirlineImage(_data.flightInfo.departure.airline,_data.flightInfo.airlines),
				departure: _data.flightInfo.departure.departure,
				arrival: _data.flightInfo.departure.arrival,
				totalDuration: _data.flightInfo.departure.totalDuration,
				connections:  parseConnection(_data.flightInfo.departure.connections,_data.flightInfo.places,_data.flightInfo.airlines)
			},
			returning: {
				origin: parsePlace(_data.flightInfo.returning.origin,_data.flightInfo.places),
				destination: parsePlace(_data.flightInfo.returning.destination,_data.flightInfo.places),
				airline: parseAirline(_data.flightInfo.returning.airline,_data.flightInfo.airlines),
				image: getAirlineImage(_data.flightInfo.returning.airline,_data.flightInfo.airlines),
				departure: _data.flightInfo.returning.departure,
				arrival: _data.flightInfo.returning.arrival,
				totalDuration: _data.flightInfo.returning.totalDuration,
				connections:  parseConnection(_data.flightInfo.returning.connections,_data.flightInfo.places,_data.flightInfo.airlines)
			}
		},
		prices: prices,
		additionals: {
			assistency: assistanceIt,
			transfer: tranferIt
		},
		fareDescription: "Total Vuelo más Hotel",
		productType: 'VUELO+HOTEL',
		deeplink:(_parameters.deeplink) ? _parameters.deeplink + '&source=crm' : ''
	};
}

module.exports = {
	parseFlightsPlusHotels: parseFlightsPlusHotels,
	parsePricingFlightsPlusHotels: parsePricingFlightsPlusHotels,
	parseQuotation:parseQuotation,
	parseBlacklist:parseBlacklist,
	parseRateRules:parseRateRules,
	parseCheckoutInformation:parseCheckoutInformation
};

function generateRoomGroup(_parameters){
	var roomGroup = {
		room:[]
	};
	for(var i=1;i<=8;i++){
		if(_parameters['room'+i]){
			roomGroup.room.push({rateKey:i});
		}
	}
	return roomGroup;
}

function parseCheckoutInformation(_result,_parameters){
	return {
		hotelRoomResponse:{
			rateInfos: {
				rateInfo: {
					chargeableRateInfo:{
						nightlyRateTotal:_result.checkoutDetailDTO.baseRate,
						surchargeTotal:_result.checkoutDetailDTO.taxes,
						total:parseFloat(_result.checkoutDetailDTO.totalPricing),
						nightlyRateTotalCLP:_result.checkoutDetailDTO.baseRate * _result.rate,
						surchargeTotalCLP:_result.checkoutDetailDTO.taxes * _result.rate,
						totalCLP: parseFloat(_result.checkoutDetailDTO.totalPricing) * _result.rate						
					},
					cancellationPolicy:'',
					roomGroup:generateRoomGroup(_parameters)
				}
			},
			rateCode:_parameters.roomTypeId,
			roomTypeCode:_parameters.roomTypeId,
			rateDescription:'',
			bedTypes:{
				size:1,
				bedType: [{
					id:_parameters.roomTypeId,
					description:'roomTypeId'
				}]
			}
		},
		rate:_result.rate,
		hotelIdSearch:_parameters.hotelID,
		region: _result.region || null,
		arrivalDate:moment(_result.params.arrivalDate).format('MM/DD/YYYY') ,
		departureDate: moment(_result.params.departureDate).format('MM/DD/YYYY'),		
		hotelName: _result.checkoutDetailDTO.hotelName,
		hotelAddress: _result.checkoutDetailDTO.address,
		checkInInstructions:'',
		supplier: 'TRV',
		hotelImages: {
			size:0
		},
		customerSessionId:''
	};
}

function getRates(_hotelRate, _referencePrice, _exchangeRate) {
	let rate = _hotelRate.rate;
	let discount = _hotelRate.discount;
	if(_referencePrice.currency === 'CLP'){
		rate = toCLP(rate,_exchangeRate);
		discount = toCLP(discount,_exchangeRate);
	}
	return {
		rate: rate + _referencePrice.totalBase,
		discount: discount,
		currency:_referencePrice.currency,
		supplier:_hotelRate.supplier
	};
}

function toCLP(_value,_exchangeRate){
	if(isNaN(_value) || isNaN(_exchangeRate) ){
		throw {
			message:'invalid value in clp conversion'
		};
	} else {
		var converted = parseFloat(_value)*parseFloat(_exchangeRate);
		return Math.round(converted);
	}
}

function parseHotelPricing(_data, _hotelDetails, _parameters) {
	let surcharges = _.cloneDeep(_data.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.surcharges);
	delete _data.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo;
	delete _data.hotelRoomResponse.rateInfos.rateInfo.taxRate;
	_data.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo = {
		surcharges: (surcharges || {}).surcharge || 0
	};
	let isKuality = (_parameters.hotels.supplier === 'TRV') ? true : false;

	var includesList = [];
	if (!isKuality && _.has(_data, 'hotelRoomResponse.valueAdds.valueAdd') && _data.hotelRoomResponse.valueAdds.valueAdd.length > 0) {
		_data.hotelRoomResponse.valueAdds.valueAdd.forEach(function(value) {
			includesList.push(value.description);
		});
	}

	var response = {
		searchId: _data.hotelIdSearch,
		region: _data.region,
		checkin: moment(_data.arrivalDate, 'MM/DD/YYYY').format(),
		checkout:  moment(_data.departureDate, 'MM/DD/YYYY').format(),
		room: _data.hotelRoomResponse,
		name:  _data.hotelName,
		address:  _data.hotelAddress,
		includes: includesList.join(', ') || null,
		amenities: _data.hotelRoomResponse.amenities || null,
		hotelAmenities: _hotelDetails.hotelAmenities || null,
		checkInInstructions:  _data.checkInInstructions,
		supplier:  _data.supplier,
		hotelFees:  (_.has(_data, 'hotelRoomResponse.rateInfos.rateInfo.hotelFees.hotelFee')) ? _data.hotelRoomResponse.rateInfos.rateInfo.hotelFees.hotelFee :null,
		mandatoryFees: (_.has(_data, 'hotelDetails.mandatoryFeesDescription')) ? _data.hotelDetails.mandatoryFeesDescription :null,
		tripAdvisorUrl: encodeURI(_hotelDetails.tripAdvisorUrl),
		coordinates: _hotelDetails.coordinates
	};

	if (_parameters.quote) {
		response.checkInTime = _data.hotelDetails.checkInTime;
		response.checkOutTime = _data.hotelDetails.checkOutTime;
		response.tripAdvisorRating = _data.tripAdvisorRating;
		response.tripAdvisorReviewCount = _data.tripAdvisorReviewCount;
		response.rateOccupancyPerRoom = _data.hotelRoomResponse.rateOccupancyPerRoom;
	}

	return response;
}

function getPricing(_flight, _hotel, _exchangeRate, _parameters) {

	let localTaxUsd = (_hotel.hotelCountry && _hotel.hotelCountry==='CL') ? _hotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.total * 0.19 : 0;
	let localTaxClp = (_hotel.hotelCountry && _hotel.hotelCountry==='CL') ? Math.round(_hotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.totalCLP * 0.19) : 0;

	return {
		usd: {
			base: _flight.prices.usd.summary.base + _hotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.nightlyRateTotal,
			taxes: _flight.prices.usd.summary.taxes + _hotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.surchargeTotal,
			fee: _flight.prices.usd.summary.fee,
			feeB2b: _flight.prices.usd.summary.feeB2b,
			localTax: localTaxUsd,
			total: _flight.prices.usd.summary.total + _hotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.total
		},
		clp: {
			base: _flight.prices.clp.summary.base + _hotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.nightlyRateTotalCLP,
			taxes: _flight.prices.clp.summary.taxes + _hotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.surchargeTotalCLP,
			fee: _flight.prices.clp.summary.fee,
			feeB2b: _flight.prices.clp.summary.feeB2b,
			localTax: localTaxClp,			
			total: _flight.prices.clp.summary.total + _hotel.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.totalCLP
		}
	};
}

function parseBlacklist(_data){
	var blacklist = {};
	for(var i=0;i<_data.length;i++){
		blacklist[_data[i].id_ean] = _data[i];
	}
	return blacklist;
}

function parseRateRules(_data){
	var rateRules = {};
	for(var i=0;i<_data.length;i++){
		rateRules[_data[i].id_ean] = _data[i];
	}
	return rateRules;
}

function parseTransfer(_data, _exchangeRate, _pax) {
	_data.pvp = _data.pvp || 0;
	let response = {
		isTransferAvailable: (_data.provider)? true : false,
		provider: _data.provider,
		type: _data.transfersType,
		contact: _data.phoneContact,
		baggage: _data.baggageIncluded,
		location: _data.locationTransfers,
		cancellationPolicy: _data.cancellationPolicy,
		requiredDocuments: _data.requiredDocuments,
		babyChairs: _data.babyChairs,
		prices: {
			usd: {
				currency: 'USD',
				perPerson: _data.pvp,
				total: _data.pvp * _pax
			},
			clp: {
				currency: 'CLP',
				perPerson: (_data.pvp * _exchangeRate),
				total: (_data.pvp * _pax) * _exchangeRate
			}
		}
	};
	return response;
}

function parseRoomsQuote(_params){
	let room,roomArr,roomData;
	let response = [];
	for (var numRoom = 1; numRoom < 4; numRoom++) {
		room = _params['room' + numRoom];
		if (room) {
			roomArr = room.split(',');
			roomData = {
				adults:roomArr[0],
				children:roomArr.length - 1,
				ages:(roomArr.length > 1) ? room.substring(2) : null
			};
			response.push(roomData);
		}
	}
	return response;
}

function parsePlace(_place,_places){
   let place = (_places[_place]) ? _places[_place] : _place;
   _.each(place,function(value,key){
	  place[key] = fixedEncodeURIComponent(value);
   });
   return place;
}

function fixedEncodeURIComponent(str) {
  if(!str){
  	return '';
  }
  return str.replace(/[!'()*]/g, function(c) {
    return '';
  });
}

function parseAirline(_airline,_airlines){
   return (_airlines[_airline]) ? _airlines[_airline].name : _airline;
}

function getAirlineImage(_airline,_airlines){
	return (_airlines[_airline]) ? _airlines[_airline].icon : 'https://cms.cocha.com/images/mobile/airlines/default.png';
}

function parseConnection(_connections,_places,_airlines){
	if(!_connections){
		return [];
	}
	for(var i=0; i < _connections.length; i++) {
		_connections[i].image			= getAirlineImage(_connections[i].airline,_airlines);
		_connections[i].airline			= parseAirline(_connections[i].airline,_airlines);
		_connections[i].origin			= parsePlace(_connections[i].origin,_places);
		_connections[i].destination		= parsePlace(_connections[i].destination,_places);
		_connections[i].cabinType		= (_.isNull(_connections[i].cabinType))? '' : _connections[i].cabinType;
		_connections[i].stopDuration	= (_.isNull(_connections[i].stopDuration))? '' : _connections[i].stopDuration;
		_connections[i].airportChange   = (_.isNull(_connections[i].airportChange))? '' : _connections[i].airportChange;
		
	}
	return _connections;
}
