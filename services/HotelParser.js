/**
 * HotelParser.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */
let externalServices 		= require('cocha-external-services');
let hotelServices 			= externalServices.hotelServices;
let blacklistModel			= require('../models/redis/Blacklist');
let rateRulesModel			= require('../models/redis/RateRules');


function filterStandard(_hotel,_parameters){
 	try {
 		return (  (isNaN(parseInt(_hotel.hotelRating)) || parseFloat(_hotel.hotelRating) < 3 )
 			 || (isNaN(parseInt(_hotel.tripAdvisorRating)) || (parseFloat(_hotel.tripAdvisorRating) < 3 && parseFloat(_hotel.tripAdvisorRating) > 0 )  )
 			); 
 	}
 	catch (err) {
 		console.log("[HotelsParser] error while rating data");
 		return false;
 	}
}

function getTotalNights(_arrivalDate,_departureDate){
	var arrivalDate = moment(_arrivalDate);
	var departureDate = moment(_departureDate);
	return Math.round(departureDate.diff(arrivalDate, 'days',true));
}

function getRateParameters(_params){
	var passengers = 0;
	var room = null;
	var roomsCount = 0;
	var adultsCount = 0;
	//console.log("PARAMS",_params);
	for (var i = 0; i < 8; i++) {
		if (isValidRoom(_params, i)) {
			room = _params['room' + i].toString().split(',');
			passengers += parseInt(room[0], 10);
			adultsCount += parseInt(room[0], 10);
			if (room.length > 1) {
				passengers += (room.length - 1);
			}
			roomsCount++;
		}
	}
	return {
		passengers: passengers,
		rooms: roomsCount,
		nights: getTotalNights(_params.arrivalDate,_params.departureDate),
		date:_params.arrivalDate.replace(/-/g, ""),
		channel:_params.channel
	};
}

let getRates = (_supplierRates, _parameters, _countryCode ,  _rateRules) => {
	var minTotal = 5000000;
	var min = 5000000;
	var maxBaseRate = 0;
	var minSurcharge = 0;	
	var minSupplier = 'EAN';
	var total = 0;
	var surchargeTotal = 0;
	var baseRate = 0;
	var avgRate = 0;
	if(_supplierRates.length == 0){
		throw {
			 status:500
			,message:'No supplier rates'
		};
	}
	for(var i=0;i<_supplierRates.length;i++){
		surchargeTotal = ((_supplierRates[i].surchargeTotal) ? parseFloat(_supplierRates[i].surchargeTotal):0);
		baseRate = parseFloat(_supplierRates[i].averageBaseRate);
		if(baseRate > maxBaseRate) {
			maxBaseRate = baseRate;
		}
		avgRate = (_supplierRates[i].averageRate) ? parseFloat(_supplierRates[i].averageRate) : parseFloat(_supplierRates[i].averageBaseRate);
		total = (avgRate + parseFloat(surchargeTotal));
		//console.log("HOTEL RATE TOTAL + TAXES N�",(i+1),total);
		//console.log("HOTEL RATE TOTAL N�",(i+1),avgRate);
		//console.log("AVG RATE",avgRate);
		//console.log("AVG BASE RATE",baseRate);
		//console.log("SUPPLIER",_supplierRates[i].availability.supplierCode);		
		//console.log("SURCHARGE",surchargeTotal);
		if (total === minTotal && !surchargeTotal && (minSupplier==='TRV' || _supplierRates[i].supplierCode==='TRV')) {
			minTotal 	 = avgRate;
			min 		 = avgRate; 
			minSurcharge = 0;
			minSupplier  = 'TRV';
			//console.log("CASO",avgRate,minSupplier,_supplierRates[i].availability.supplierCode);
		} else if(total < minTotal) {
			minTotal = (avgRate + parseFloat(surchargeTotal));
			min = avgRate; 
			minSurcharge = surchargeTotal;
			minSupplier = _supplierRates[i].supplierCode;
		} 
	}
	//console.log("discount",(maxBaseRate - min));
	//console.log("min supplier",minSupplier);
	var referenceHotelPrice = getIndividualRate(maxBaseRate,minSurcharge, _parameters, _countryCode);
	var individualRate = getIndividualRate(min,minSurcharge, _parameters,_countryCode);

	if(_rateRules) {
		//console.log("OVERRULE",_rateRules,_parameters);
		if( (_rateRules.supplier !== minSupplier) && ((_parameters.channel==='B2B' && (_rateRules.canal==='b2b' || _rateRules.canal==='all')) ||  (_parameters.channel==='B2C' && (_rateRules.canal==='b2c' || _rateRules.canal==='all')))){
			var index = _.findIndex(_supplierRates, function(o) { return (o ? o.supplierCode === _rateRules.supplier : -1 );});
			if(index > -1){
				minSupplier 		= _rateRules.supplier;
				min 				= (_supplierRates[index].averageRate) ? parseFloat(_supplierRates[index].averageRate) : parseFloat(_supplierRates[index].averageBaseRate);
				minSurcharge		= ((_supplierRates[index].surchargeTotal) ? parseFloat(_supplierRates[index].surchargeTotal):0);
				individualRate 		= getIndividualRate(min,minSurcharge, _parameters, _countryCode);
				referenceHotelPrice	= individualRate;//to make it zero
				/*
				console.log("HOTEL AVG RATE ", min);
				console.log("HOTEL AVG RATE TOTAL + TAXES ELEGIDO", min, minSurcharge);
				console.log("HOTEL MIN SUPPLIER ", minSupplier);	
				console.log("HOTEL RATE PARAMETERS", _parameters);
				console.log("HOTEL INDIVIDUAL RATE ", getIndividualRate(min, minSurcharge, _parameters));
				*/
			}
		}
	}

	/*
	console.log("HOTEL AVG RATE ", min);
	console.log("HOTEL AVG RATE TOTAL + TAXES ELEGIDO", min, minSurcharge);
	console.log("HOTEL MAX BASE RATE ", maxBaseRate);
	console.log("HOTEL MIN SUPPLIER ", minSupplier);	
	console.log("HOTEL RATE PARAMETERS", _parameters);
	console.log("HOTEL INDIVIDUAL RATE ", getIndividualRate(min, minSurcharge, _parameters));
	*/
	return {
		rate: individualRate,
		discount: (referenceHotelPrice - individualRate),
		supplier:getSupplier(minSupplier)
	};
};

function getSupplier(_supplier){
	switch(_supplier){
		case 'EAN':
		return '1';
		case 'EANPKGR':
		return '2';
		case 'TRV':
		return '3';
		default:
		return '1';
	}	
}

let getIndividualRate = (_rate, _surcharge , _parameters, _countryCode) => {
	let tax = (_countryCode && _countryCode === 'CL' ? 1.19 : 1 );
	return (  (((parseFloat(_rate) * _parameters.nights * _parameters.rooms)  + _surcharge) * tax)  / _parameters.passengers);
};

let getTotal = (_rate, _surcharge , _parameters, _countryCode) => {
	let tax = (_countryCode && _countryCode === 'CL' ? 1.19 : 1 );
	return (((parseFloat(_rate) * _parameters.nights * _parameters.rooms)  + _surcharge) * tax);
};


let isValidRoom = (_params, _index) => {
	return (_params.hasOwnProperty('room' + _index) && _params['room' + _index] !== null && _params['room' + _index] !== '') ;		
};

function isBlacklisted(_hotelId,_channel,_blacklist) {
	return (_blacklist[_hotelId] && ( (_channel==="B2B" && _blacklist[_hotelId].btob==='1') || (_channel==="B2C" && _blacklist[_hotelId].btoc==='1') ) ) ;
}

function* parseHotels(_hotels, _parameters) {
	var rateParameters = getRateParameters(_parameters);
	var blacklist	   = yield blacklistModel.getBlacklist();
	var rateRules      = yield rateRulesModel.getRateRules();
	var i = _hotels.found.length || 0;
	while (i--) {
		if(filterStandard(_hotels.found[i],_parameters)){
			_hotels.found.splice(i, 1);
			continue;
		}		
		if(isBlacklisted(_hotels.found[i].hotelId,_parameters.channel,blacklist)){
			_hotels.found.splice(i, 1);
			continue;
		}		

		//console.log("HOTEL NAME",_hotels.found[i].name);
		_hotels.found[i].rate = getRates(_hotels.found[i].rates, rateParameters, _hotels.found[i].countryCode, rateRules[_hotels.found[i].hotelId]);
		_hotels.found[i].categories = _hotels.found[i].categories;
		_hotels.found[i].ranking = i;

		delete _hotels.found[i].rates;
	}
	return _hotels;
}

function toCLP(_value,_exchangeRate){
	if(isNaN(_value) || isNaN(_exchangeRate) ){
		throw {
			message:'invalid value in clp conversion'
		};
	} else {
		var converted = parseFloat(_value)*parseFloat(_exchangeRate);
		return Math.round(converted);
	}
}

function* parseHotelDetail(_result, _parameters, _delta) {
	let rawRoomsPrices = {
		rooms: []
	};

	_result.hotelDetail.nearPlaces = _result.pointsInterest;
	let rateParameters = getRateParameters(_parameters);
	var surchargeTotal = 0;
	let roomPrice;

	for (var i = 0; i < _result.hotelDetail.rooms.length; i++) {
		surchargeTotal = (_result.hotelDetail.rooms[i].hasOwnProperty('surchargeTotal') && _result.hotelDetail.rooms[i].surchargeTotal !== null) ? parseFloat(_result.hotelDetail.rooms[i].surchargeTotal) : 0;
		roomPrice = {
			roomRateCode: _result.hotelDetail.rooms[i].rateCode,
			roomTypeCode: _result.hotelDetail.rooms[i].roomId,
			roomName: _result.hotelDetail.rooms[i].name,
			priceBase: {
				price: _result.hotelDetail.rooms[i].price,
				surchargeTotal: surchargeTotal
			}
		};
		//console.log("ROOM NAME", _result.hotelDetail.rooms[i].name);
		//console.log("PRICE H", _result.hotelDetail.rooms[i].price);
		//console.log("TAX H", surchargeTotal);
		//console.log("BASE USD FLIGHTS", _delta.usd.totalBase);
		var originalPrice = _.cloneDeep(_result.hotelDetail.rooms[i].price);
		var exchangeRate  = _result.hotelDetail.rooms[i].exchangeRate;
		var total = getTotal(_result.hotelDetail.rooms[i].price, surchargeTotal, rateParameters, _result.hotelDetail.info.countryCode);
		_result.hotelDetail.rooms[i].price = getIndividualRate(_result.hotelDetail.rooms[i].price, surchargeTotal, rateParameters, _result.hotelDetail.info.countryCode);

		_result.hotelDetail.rooms[i].rates = {
			usd: {
				currency: _delta.usd.currency,
				price: _result.hotelDetail.rooms[i].price + _delta.usd.totalBase,
				total: total + (_delta.usd.totalBase * rateParameters.passengers),
			},
			clp: {
				currency: _delta.clp.currency,
				//price: Math.round(getIndividualRate(toCLP(originalPrice, exchangeRate), toCLP(surchargeTotal, exchangeRate), rateParameters)) + _delta.clp.totalBase
				price: toCLP(getIndividualRate(originalPrice,surchargeTotal, rateParameters, _result.hotelDetail.info.countryCode),exchangeRate) + _delta.clp.totalBase,
				total: toCLP(total,exchangeRate) + (_delta.clp.totalBase * rateParameters.passengers),
			}
		};
		roomPrice.priceRefactor = _result.hotelDetail.rooms[i].rates;
		rawRoomsPrices.rooms.push(roomPrice);
		_result.hotelDetail.rooms[i].amenities = parseAmenities(_result.hotelDetail.rooms[i].amenities,_result.hotelDetail.info.amenities);
		_result.hotelDetail.rooms[i].currency = _delta.usd.currency;
		_result.hotelDetail.rooms[i].price += _delta.usd.totalBase;
	}
	return yield {
		hotelDetail: _result.hotelDetail,
		dataPrices: rawRoomsPrices
	};
}

function parseAmenities(_roomAmenities,_hotelAmenities){
	var allInclusive = _.find(_hotelAmenities, ['TYPE', 'ALL-INCLUSIVE']);
	if(_roomAmenities) {
		var i =_roomAmenities.length || 0;
		var foundBreakfast=false;
		while (i--) {
			if (_roomAmenities[i].name === 'BREAKFAST') {
				foundBreakfast = true;
				//if (allInclusive) {
				  //_roomAmenities[i].text = "Todo incluido";
				  //_roomAmenities[i].name = "ALL-INCLUSIVE";
				  //_roomAmenities[i].included = true;
				//} else {
					if (_roomAmenities[i].text) {
						if (_roomAmenities[i].text.toLowerCase().indexOf("todo incluido") > -1) {
							_roomAmenities[i].name = 'ALL-INCLUSIVE';
						}
						else if (_roomAmenities[i].text.toLowerCase().indexOf("media") > -1) {
							_roomAmenities[i].name = 'HALF-PENSION';
						}
						else if (_roomAmenities[i].text.toLowerCase().indexOf("completa") > -1 || _roomAmenities[i].text.toLowerCase().indexOf("todas las comidas") > -1) {
							_roomAmenities[i].name = 'FULL-PENSION';
						}
					}
				//}
			} else {
				_roomAmenities.splice(i, 1);
			}
		}
		if(foundBreakfast){
			return _roomAmenities;
		} else {
			if (allInclusive) {
				//var amenity = {
				//  text : "Todo incluido",
				//  name : "ALL-INCLUSIVE",
				//  included : true
				//};
				_roomAmenities = [];
				//_roomAmenities.push(amenity);
				return _roomAmenities;
			} else {
				return [];
			}
		}
	} else {
		if (allInclusive) {
			//var amenity = {
			  //text : "Todo incluido",
			  //name : "ALL-INCLUSIVE",
			  //included : true
			//};
			_roomAmenities = [];
			//_roomAmenities.push(amenity);
			return _roomAmenities;
		} else {
			return [];
		}		
	}
}

module.exports = {
	 parseHotels: parseHotels
	,parseHotelDetail: parseHotelDetail
	,getRates:getRates
	,getRateParameters:getRateParameters	
};