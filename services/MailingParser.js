/**
    MailingParser.js
/* jshint strict: false, esversion: 6 */
"use strict";

let handlebars = require('handlebars');
let moment = require('moment');
moment.locale('es');

let fs = require('fs');

let logger = require('@cocha/cocha-logger-services')('email-parser');
let pdfGeneratorService = require('./PdfGeneratorService');
let oportunityModel = require('../models/redis/Oportunity');
let sendMailService = require('cocha-external-services').sendMail;
const ENV = process.env.NODE_ENV || 'production';

function convertUSDtoCLP(valueExchange, amountExchange) {
    if (amountExchange > 0) {
        return Math.floor(amountExchange * valueExchange)
    } else return 0;

}

function* parsePdfVoucher(_params) {
    let logObj = {
        function: "parsePdfVoucherFlightHotel"
        , params: _params
        , success: true
        , level: 'info'
    };

    let global_merge_vars = yield parseGlobalMergeVars(_params);
    let templateVoucherScript = fs.readFileSync('views/templates/voucher.handlebars', 'utf8');

    // Category image
    handlebars.registerHelper('hotelCatImg', function () {
        let hotelRating = _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].hotelRating;
        return getHotelRatingImageTag(hotelRating);
    });
    // capitalize text
    handlebars.registerHelper('capitalize', function (input) {
        if (!_.isString(input)) {
            return input;
        }
        let output = input.substring(0, 1).toUpperCase() + input.substring(1);
        return output.replace(/ *\([^)]*\) */g, "");
    });
    // upper text
    handlebars.registerHelper('upper', function (input) {
        if (!_.isString(input)) {
            return input;
        }
        let output = input.toUpperCase();
        return output.replace(/ *\([^)]*\) */g, "");
    });
    // format currency
    handlebars.registerHelper('priceFilter', function (val, currency) {
        let currencySymbol;
        if (currency === 'USD') {
            currencySymbol = 'US$';
        }
        else if (currency === 'CLP') {
            currencySymbol = '$';
        }
        else {
            currencySymbol = currency;
        }
        if (!isNaN(val)) {
            return currencySymbol + Math.ceil(val).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&.');
        }
        else {
            return val;
        }
    });

    let templateVoucherHtml = handlebars.compile("{{#content}}" + templateVoucherScript + "{{/content}}");
    let compiledVoucherHtml = templateVoucherHtml(global_merge_vars);

    let pdfStream = yield pdfGeneratorService.generatePdfFromStringToStream(compiledVoucherHtml);

    return pdfStream;
}

function* parseGlobalMergeVars(_params) {
    let logObj = {
        function: "parseGlobalMergeVars",
        params: _params,
        success: true,
        level: 'info'
    };

    let _config_hotelConfirmation = _params.hotel.itinerary.itinerary.hotelConfirmation;
    let _config_hotelNotes = _params.hotel.itinerary.itinerary.hotelConfirmation[0].valueAdds || [];
    let _config_flights = _params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO;

    //rooms
    let config_rooms = [];
    _.each(_config_hotelConfirmation, function (_config_hotelConfirmation, i) {
        config_rooms.push({
            id: i + 1,
            confirmationNumber: _params.hotel.itinerary.itinerary.hotelConfirmation[0].confirmationNumber,
            adultCount: _config_hotelConfirmation.numberOfAdults,
            childCount: _config_hotelConfirmation.numberOfChildren,
            roomType: _config_hotelConfirmation.roomDescription,
            nights: _config_hotelConfirmation.nights,
            reservationGuest: [
                {
                    firstName: _config_hotelConfirmation.reservationGuest.firstName,
                    lastName: _config_hotelConfirmation.reservationGuest.lastName,
                    rut: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].docNumber,
                }
            ],
        });
    });

    //rooms
    let amountHotel = 0;
    _.each(_config_hotelConfirmation, function (x) {
        amountHotel = amountHotel + parseFloat(x.rateInfos.rateInfo.chargeableRateInfo.total)
    });

    //notas del hotel
    let config_hotel_notes = [];
    _.each(_config_hotelNotes.valueAdd, function (_config_hotelNotes) {
        config_hotel_notes += _config_hotelNotes.description + ". ";
    });

    // Flight Departure Con
    // let _config_flightDetCon = _.find(_config_flights, function (obj) { if (obj.connType !== null  && obj.directionFlag === "O") { return obj; } });

    let _config_flightDepDep = _.find(_config_flights, function (obj) { if ((obj.connType === "O" || obj.connType === null) && obj.directionFlag === "O") { return obj; } });
    let _config_flightDepCon = _.filter(_config_flights, function (obj) {
        if ((obj.connType !== null && obj.connType !== "O") && obj.directionFlag === "O") {
            obj.flightNumber = obj.airlineCode + parseInt(obj.airlineNumber).toString();
            obj.cityCode = obj.cityOrigin.cityCode;
            obj.departureD = moment(obj.departureDateTime).format("DD");
            obj.departureMMM = moment(obj.departureDateTime).format('MMM');
            obj.departureHHMM = moment(obj.departureDateTime).format("HH:mm");
            return obj;
        }
    });

    let _config_flightDepArr = _.last(_.filter(_config_flights, function (obj) { if (obj.directionFlag === "O") { return obj; } }));

    // Flight Departure
    let config_flightDep = [];
    config_flightDep.push({
        cabinType: _config_flightDepDep.cabinType,
        elapsedTime: _config_flightDepDep.elapsedTime,
        airlineCode: _config_flightDepDep.airlineName,
        airlineNumber: _config_flightDepDep.airlineNumber,
        airlineName: _config_flightDepDep.airlineName,
        airlineModel: _config_flightDepDep.airlineModel,
        segment: _config_flightDepDep.segment,
        connections: _config_flightDepCon || [],
        origin: [
            {
                flightNumber: _config_flightDepDep.airlineCode + parseInt(_config_flightDepDep.airlineNumber).toString(),
                cityCode: _config_flightDepDep.cityOrigin.cityCode,
                cityName: _config_flightDepDep.cityOrigin.cityName,
                countryCode: _config_flightDepDep.cityOrigin.countryCode,
                countryName: _config_flightDepDep.cityOrigin.countryName,
                terminal: _config_flightDepDep.departureTerminal,
                departureDateTime: _config_flightDepDep.departureDateTime,
                departureLongDate: moment(_config_flightDepDep.departureDateTime).format("ddd DD MMMM"),
                departureHHMM: moment(_config_flightDepDep.departureDateTime).format("HH:mm"),
                departureD: moment(_config_flightDepDep.departureDateTime).format("DD"),
                departureWD: moment(_config_flightDepDep.departureDateTime).format('ddd'),
                departureMMM: moment(_config_flightDepDep.departureDateTime).format('MMM'),
            }
        ],
        destination: [
            {
                cityCode: _config_flightDepArr.cityDestination.cityCode,
                cityName: _config_flightDepArr.cityDestination.cityName,
                countryCode: _config_flightDepArr.cityDestination.countryCode,
                countryName: _config_flightDepArr.cityDestination.countryName,
                terminal: _config_flightDepArr.arrivalTerminal,
                arrivalDateTime: _config_flightDepArr.departureDateTime,
                arrivalLongDate: moment(_config_flightDepArr.departureDateTime).format("ddd DD MMMM"),
                arrivalHHMM: moment(_config_flightDepArr.arrivalDateTime).format("HH:mm"),
                arrivalD: moment(_config_flightDepArr.arrivalDateTime).format("DD"),
                arrivalWD: moment(_config_flightDepArr.arrivalDateTime).format('ddd'),
                arrivalMMM: moment(_config_flightDepArr.arrivalDateTime).format('MMM'),
            }
        ]
    });

    // Flight Return Con
    let _config_flightRetDep = _.find(_config_flights, function (obj) { if ((obj.connType === "O" || obj.connType === null) && obj.directionFlag === "R") { return obj; } });
    let _config_flightRetCon = _.find(_config_flights, function (obj) {
        if ((obj.connType !== null && obj.connType !== "O") && obj.directionFlag === "R") {
            obj.flightNumber = obj.airlineCode + parseInt(obj.airlineNumber).toString();
            obj.cityCode = obj.cityOrigin.cityCode;
            obj.departureD = moment(obj.departureDateTime).format("DD");
            obj.departureMMM = moment(obj.departureDateTime).format('MMM');
            obj.departureHHMM = moment(obj.departureDateTime).format("HH:mm");
            return obj;
        }
    });
    let _config_flightRetArr = _.find(_config_flights, function (obj) { if ((obj.connType === "I" || obj.connType === null) && obj.directionFlag === "R") { return obj; } });

    // Flight Return
    let config_flightRet = [];
    config_flightRet.push({
        cabinType: _config_flightRetDep.cabinType,
        elapsedTime: _config_flightRetDep.elapsedTime,
        airlineCode: _config_flightRetDep.airlineName,
        airlineNumber: _config_flightRetDep.airlineNumber,
        airlineName: _config_flightRetDep.airlineName,
        airlineModel: _config_flightRetDep.airlineModel,
        segment: _config_flightRetDep.segment,
        connections: _config_flightRetCon,
        origin: [
            {
                flightNumber: _config_flightRetDep.airlineCode + parseInt(_config_flightRetDep.airlineNumber).toString(),
                cityCode: _config_flightRetDep.cityOrigin.cityCode,
                cityName: _config_flightRetDep.cityOrigin.cityName,
                countryCode: _config_flightRetDep.cityOrigin.countryCode,
                countryName: _config_flightRetDep.cityOrigin.countryName,
                terminal: _config_flightRetDep.departureTerminal,
                departureDateTime: _config_flightRetDep.departureDateTime,
                departureLongDate: moment(_config_flightRetDep.departureDateTime).format("ddd DD MMMM"),
                departureHHMM: moment(_config_flightRetDep.departureDateTime).format("HH:mm"),
                departureD: moment(_config_flightRetDep.departureDateTime).format("DD"),
                departureWD: moment(_config_flightRetDep.departureDateTime).format('ddd'),
                departureMMM: moment(_config_flightRetDep.departureDateTime).format('MMM'),
            }
        ],
        destination: [
            {
                cityCode: _config_flightRetArr.cityDestination.cityCode,
                cityName: _config_flightRetArr.cityDestination.cityName,
                countryCode: _config_flightRetArr.cityDestination.countryCode,
                countryName: _config_flightRetArr.cityDestination.countryName,
                terminal: _config_flightRetArr.arrivalTerminal,
                arrivalDateTime: _config_flightRetArr.departureDateTime,
                arrivalLongDate: moment(_config_flightRetArr.departureDateTime).format("ddd DD MMMM"),
                arrivalHHMM: moment(_config_flightRetArr.arrivalDateTime).format("HH:mm"),
                arrivalD: moment(_config_flightRetArr.arrivalDateTime).format("DD"),
                arrivalWD: moment(_config_flightRetArr.arrivalDateTime).format('ddd'),
                arrivalMMM: moment(_config_flightRetArr.arrivalDateTime).format('MMM'),
            }
        ]
    });

    //Google Schemas
    let _googleSchemaHotel = parseGoogleSchemaHotel(_params);
    //let _googleSchemaFlight = parseGoogleSchemaFlight(_params);

    let checkIn = moment(_params.hotel.itinerary.itinerary.hotelConfirmation[0].arrivalDate, 'MM/DD/YYYY').format("dddd DD MMMM");
    let checkOut = moment(_params.hotel.itinerary.itinerary.hotelConfirmation[0].departureDate, 'MM/DD/YYYY').format("dddd DD MMMM");
    let paymentInfo;
    let flagAssistance = false;
    let flagTransfer = false;
    let flagService = false;
    if (_.has(_params.payment, 'payAmount')) {
        if (Object.keys(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO).length === 0 || !_params.hotel.details || Object.keys(_params.hotel.itinerary.itinerary.hotelConfirmation).length === 0 || !_params.priceFH) {
            logObj.success = false;
            logObj.recipient_to = _params.flight.itinerary.contactEmail;

            if (Object.keys(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO).length === 0) {
                logObj.message = 'Error during parsing [parseConfirmationFlightHotel]: Missing booking flight info data';
                let msg = 'Falta información de confirmación del vuelo';
            } else if (!_.has(_params.hotel.itinerary)) {
                logObj.message = 'Error during parsing [parseConfirmationFlightHotel]: Missing hotel confirmation info data';
                let msg = 'Falta información de confirmación del hotel';
            } else if (!_params.hotel.details) {
                logObj.message = 'Error during parsing [parseConfirmationFlightHotel]: Missing booking hotel detail info data';
                let msg = 'Falta información de confirmación del detalle del hotel';
            } else if (!_params.priceFH) {
                logObj.message = 'Error during parsing [parseConfirmationFlightHotel]: Missing booking pricing data';
                let msg = 'Falta información de confirmación del pago';
            } else {
                logObj.message = 'Error during parsing [parseConfirmationFlightHotel]: Missing other data';
                let msg = 'Falta información no identificada';
            }

            logger.log('error', logObj);
            let alerted = yield sendAlertEmailNotSent(_params, msg);
            throw {
                status: 503,
                message: { message: msg, alerted: alerted, contact_mail: _params.flight.itinerary.contactEmail },
                data: JSON.stringify(_params)
            };
        }
        let totalFee = parseFloat(_params.priceFH.totalTaxesVH || "0") + parseFloat(_params.priceFH.fee || "0");
        paymentInfo = [{
            fullName: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].firstName + ' ' + _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].lastName,
            rut: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].docNumber,
            telephone: _params.hotel.itinerary.itinerary.customer.homePhone,
            email: _params.hotel.itinerary.itinerary.customer.email,
            amounts: [
                {
                    hotelUSD: _params.priceFH.totalMountVH / (parseFloat((_config_hotelConfirmation.numberOfAdults || "1")) + parseFloat((_config_hotelConfirmation.numberOfChildren || "0"))),
                    tasasCargosUSD: totalFee,
                    transferUSD: _params.priceFH.transfer || "0",
                    asistenciaUSD: _params.priceFH.segyasisAmount || "0",
                    serviceFeeUSD: _params.priceFH.feeFast || "0",
                    totalUSD: parseFloat(_params.priceFH.totalMountVH) + totalFee + parseFloat(_params.priceFH.segyasisAmount || "0") + parseFloat(_params.priceFH.transfer || "0") + parseFloat(_params.priceFH.feeFast || "0"),
                    totalCLP: _params.payment.payAmount,

                    hotelCLP: convertUSDtoCLP(_params.priceFH.totalMountVH / (parseFloat((_config_hotelConfirmation.numberOfAdults || "1")) + parseFloat((_config_hotelConfirmation.numberOfChildren || "0"))), _params.priceFH.usdKuality),
                    tasasCargosCLP: convertUSDtoCLP(totalFee, _params.priceFH.usdKuality),
                    transferCLP: convertUSDtoCLP(_params.priceFH.transfer, _params.priceFH.usdKuality),
                    asistenciaCLP: convertUSDtoCLP(_params.priceFH.segyasisAmount, _params.priceFH.usdKuality),
                    serviceFeeCLP: convertUSDtoCLP(_params.priceFH.feeFast, _params.priceFH.usdKuality),
                    localTax: convertUSDtoCLP(amountHotel * 0.19, _params.priceFH.usdKuality),
                    totalCLPLocal: _params.payment.payAmount + convertUSDtoCLP(amountHotel * 0.19, _params.priceFH.usdKuality)
                }
            ],
        }];
        flagAssistance = (_params.priceFH.segyasisAmount !== null && parseInt(_params.priceFH.segyasisAmount) !== 0);
        flagTransfer = (_params.priceFH.transfer !== null && parseInt(_params.priceFH.transfer) !== 0);
        flagService = (_params.priceFH.feeFast !== null && parseInt(_params.priceFH.feeFast) !== 0);
    }
    ////parse vars
    let global_merge_vars = {
        name: "contexto",
        content: [
            {
                codReservations: [
                    {
                        codReservaVue: _params.flight.itinerary.pnr,//codReservaVue,
                        codReservaHot: _params.hotel.itinerary.itinerary.itineraryId,//codReservaHot,
                        codReservaVou: _params.hotel.itinerary.itinerary.hotelConfirmation[0].confirmationNumber,//codReservaVou,
                        codReservaInt: _params.flight.itinerary.superPnr,//codReservaInt
                    }
                ],
                customerInfo: [
                    {
                        email: _params.hotel.itinerary.itinerary.customer.email,
                        firstName: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].firstName,
                        lastName: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].lastName,
                        rut: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].docNumber,
                        homePhone: _params.hotel.itinerary.itinerary.customer.homePhone,
                        workPhone: _params.hotel.itinerary.itinerary.customer.workPhone,
                        extension: _params.hotel.itinerary.itinerary.customer.extension,
                        faxPhone: _params.hotel.itinerary.itinerary.customer.faxPhone,
                        customerAddresses: [
                            {
                                address1: _params.hotel.itinerary.itinerary.customer.customerAddresses.address1,
                                city: _params.hotel.itinerary.itinerary.customer.customerAddresses.city,
                                stateProvinceCode: _params.hotel.itinerary.itinerary.customer.customerAddresses.stateProvinceCode,
                                countryCode: _params.hotel.itinerary.itinerary.customer.customerAddresses.countryCode,
                                postalCode: _params.hotel.itinerary.itinerary.customer.customerAddresses.postalCode,
                                isPrimary: _params.hotel.itinerary.itinerary.customer.customerAddresses.isPrimary,
                                type: _params.hotel.itinerary.itinerary.customer.customerAddresses.type,
                            }
                        ]
                    }
                ],
                paymentInfo: paymentInfo,
                itineraryHotel: [
                    {
                        contactFullName: _params.hotel.itinerary.itinerary.customer.firstName + ' ' + _params.hotel.itinerary.itinerary.customer.lastName,
                        confirmationNumber: _params.hotel.itinerary.itinerary.itineraryId,
                        checkInDate: _params.hotel.itinerary.itinerary.hotelConfirmation[0].arrivalDate,
                        checkoutDate: _params.hotel.itinerary.itinerary.hotelConfirmation[0].departureDate,
                        checkInLongDate: (_params.hotel.details.checkInTime.toLowerCase() === "noon") ? checkIn + ", desde las 12 AM" : (_params.hotel.details.checkInTime === "") ? checkIn : checkIn + ", desde las " + _params.hotel.details.checkInTime,
                        checkOutLongDate: (_params.hotel.details.checkOutTime.toLowerCase() === "noon") ? checkOut + ", hasta las 12 AM" : (_params.hotel.details.checkOutTime === "") ? checkOut : checkOut + ", hasta las " + _params.hotel.details.checkOutTime,
                        adultCount: _.reduce(_params.hotel.itinerary.itinerary.hotelConfirmation, function (room, num) { return room + num.numberOfAdults; }, 0),
                        childCount: _.reduce(_params.hotel.itinerary.itinerary.hotelConfirmation, function (room, num) { return room + num.numberOfChildren; }, 0),
                        nights: _params.hotel.itinerary.itinerary.hotelConfirmation[0].nights,
                        hotelId: _params.hotel.details.id,
                        name: _params.hotel.details.name,
                        address: _params.hotel.details.address,
                        telephone: _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].phone,
                        cityName: _params.hotel.details.address.concat(". <br>Tel. ", _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].phone),
                        //cityCode			: _params.hotel.details.
                        zipCode: _params.hotel.details.zipCode,
                        countryCode: _params.hotel.details.country,
                        hotelRating: _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].hotelRating,
                        mandatoryFeeDescription: _params.hotel.details.mandatoryFeeDescription,
                        feeDescription: _params.hotel.details.feeDescription,
                        policyDescription: _params.hotel.details.policyDescription,
                        cancellationPolicy: _params.hotel.itinerary.itinerary.hotelConfirmation[0].rateInfos.rateInfo.cancellationPolicy,
                        images: _params.hotel.details.images,
                        notes: config_hotel_notes,
                        rooms: config_rooms,
                    }
                ],
                itineraryFlight: [
                    {
                        pnr: _params.flight.itinerary.pnr,
                        reservationNumber: _params.flight.itinerary.supplierRef,
                        paymentForm: _params.flight.itinerary.paymentForm,
                        totalFare: _params.flight.itinerary.totalFare,
                        basicFare: _params.flight.itinerary.basicFare,
                        equivFare: _params.flight.itinerary.equivFare,
                        taxes: _params.flight.itinerary.taxes,
                        contactFullName: _params.flight.itinerary.contactFullName,
                        contactFirstName: _params.flight.itinerary.contactFirstName,
                        contactLastName: _params.flight.itinerary.contactLastName,
                        contactEmail: _params.flight.itinerary.contactEmail,
                        contactPhone: _params.flight.itinerary.contactPhone,
                        bookStatus: _params.flight.itinerary.bookStatus,
                        bookingFlights: [
                            {
                                flightDeparture: config_flightDep,
                                flightReturn: config_flightRet
                            }
                        ]
                    }
                ],
                flagAssistance: flagAssistance,
                flagTransfer: flagTransfer,
                flagService: flagService,
                itineraryTransfer: {
                    idTransfers: 7,
                    mainPassenger: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].firstName + ' ' + _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].lastName,
                    origin: _config_flightDepArr.cityDestination.cityCode,
                    destination: _config_flightDepArr.cityDestination.cityName,
                    country:_config_flightRetArr.cityDestination.countryName,
                    hotel: _params.hotel.details.name,
                    airport: _config_flightDepArr.arrivalTerminal,
                    longDateA: moment(_config_flightDepDep.departureDateTime).format("ddd DD MMMM"),
                    longDateB: moment(_config_flightRetDep.departureDateTime).format("ddd DD MMMM"),
                    ianCode: _params.transfer.ianCode,
                    provider: _params.transfer.provider,
                    transfersType: _params.transfer.transfersType,
                    baggageIncluded:  _params.transfer.baggageIncluded,
                    phoneContact:  _params.transfer.phoneContact,
                    locationTransfers:  _params.transfer.locationTransfers,
                    cancellationPolicy:  _params.transfer.cancellationPolicy,
                    requiredDocuments: _params.transfer.requiredDocuments,
                    babyChairs:  _params.transfer.babyChairs,
                    passengers: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO,
                    flightNumberA: _config_flightDepArr.airlineCode + parseInt(_config_flightDepArr.airlineNumber),
                    flightNumberB: _config_flightRetDep.airlineCode + parseInt(_config_flightRetDep.airlineNumber)
                },
                googleSchemaHotel: _googleSchemaHotel,
                localTrip: _params.flight.itinerary.destPost.countryName === "CHILE" & _params.flight.itinerary.destPost.cityCode != "IPC"
                // googleSchemaFlight: _googleSchemaFlight
            },
        ]
    };
    return global_merge_vars;
}

function* parseConfirmationFlightHotel(_params, _email) {
    let logObj = {
        function: "parseConfirmationFlightHotel"
        , params: _params
        , success: true
        , level: 'info'
    };

    let global_merge_vars = yield parseGlobalMergeVars(_params);

    let templateConfirmationScript = fs.readFileSync('views/templates/confirmation.handlebars', 'utf8');
    let templateVoucherHotelScript = fs.readFileSync('views/templates/voucher.handlebars', 'utf8');
    let templateVoucherTransferAScript = fs.readFileSync('views/templates/transferA.handlebars', 'utf8');
    let templateVoucherTransferBScript = fs.readFileSync('views/templates/transferB.handlebars', 'utf8');

    // Category image
    handlebars.registerHelper('hotelCatImg', function () {
        let hotelRating = parseInt(_params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].hotelRating);
        return getHotelRatingImageTag(hotelRating);
    });

    // capitalize text
    handlebars.registerHelper('capitalize', function (input) {
        if (!_.isString(input)) {
            return input;
        }
        let output = input.substring(0, 1).toUpperCase() + input.substring(1);
        return output.replace(/ *\([^)]*\) */g, "");
    });

    // upper text
    handlebars.registerHelper('upper', function (input) {
        if (!_.isString(input)) {
            return input;
        }
        let output = input.toUpperCase();
        return output.replace(/ *\([^)]*\) */g, "");
    });
    // format currency
    handlebars.registerHelper('priceFilter', function (val, currency) {
        let currencySymbol;
        if (currency === 'USD') {
            currencySymbol = 'US$';
        }
        else if (currency === 'CLP') {
            currencySymbol = '$';
        }
        else {
            currencySymbol = currency;
        }
        if (!isNaN(val)) {
            return currencySymbol + Math.ceil(val).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&.');
        }
        else {
            return val;
        }
    });

    let templateVoucherHotelHtml = handlebars.compile("{{#content}}" + templateVoucherHotelScript + "{{/content}}");
    let compiledVoucherHotelHtml = templateVoucherHotelHtml(global_merge_vars);

    let templateVoucherTransferAHtml = handlebars.compile("{{#content}}" + templateVoucherTransferAScript + "{{/content}}");
    let compiledVoucherTransferAHtml = templateVoucherTransferAHtml(global_merge_vars);

    let templateVoucherTransferBHtml = handlebars.compile("{{#content}}" + templateVoucherTransferBScript + "{{/content}}");
    let compiledVoucherTransferBHtml = templateVoucherTransferBHtml(global_merge_vars);

    let templateConfirmationHtml = handlebars.compile("{{#content}}" + templateConfirmationScript + "{{/content}}", { noEscape: true }, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        }
    });

    let compiledConfirmationHtml = templateConfirmationHtml(global_merge_vars, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        };
    });

    //// Generar pdf
    let pdfHotel = yield pdfGeneratorService.generatePdfFromStringToBuffer(compiledVoucherHotelHtml);
    let pdfTransferA = yield pdfGeneratorService.generatePdfFromStringToBuffer(compiledVoucherTransferAHtml);
    let pdfTransferB = yield pdfGeneratorService.generatePdfFromStringToBuffer(compiledVoucherTransferBHtml);

    let attachmentsPDF = [{
        type: "application/pdf",
        name: "VoucherHotel.pdf",
        content: pdfHotel
    }];
    if (parseInt(_params.transfer.totalTransfers) > 0) {
        attachmentsPDF.push({
            type: "application/pdf",
            name: "VoucherTransferIda.pdf",
            content: pdfTransferA
        });
        attachmentsPDF.push({
            type: "application/pdf",
            name: "VoucherTransferVuelta.pdf",
            content: pdfTransferB
        });
    }

    let config = {
        html: compiledConfirmationHtml,
        subject: "Gracias por tu compra en Cocha.com",
        from_email: "agenteinternet@cocha.com",
        from_name: "Cocha",
        signing_domain: "cocha.com",
        to: [{
                email: _email || _params.hotel.itinerary.itinerary.customer.email,
                name: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].firstName + ' ' + _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].lastName,
                type: "to"
        }],
        important: true,
        inline_css: true,
        attachments: attachmentsPDF,
    };
    config.to = addAdditionalBccEmails(config.to);
    return config;
};

/**
 * Adds more email addresses to the "to:" emails array.
 * @param {Array} _toArr
 * @returns {Array}
 */
function addAdditionalBccEmails(_toArr) {
    if (ENV === 'production') {
        const emailAddresses = [
            'agenteinternet@cocha.com',
            'cquezada@cocha.com',
            'jmpaz@cocha.com',
            'fgargiulo@cocha.com',
            'pramirez@cocha.com',
            'pvega@cocha.com',
            'cleon@cocha.com',
            'nmardones@cocha.com',
        ];
        emailAddresses.forEach((address) => {
            _toArr.push({
                email: address,
                type: 'bcc'
            });
        });
    }
    return _toArr;
}

function* parseConfirmationFlightHotelWithIds(_params, _email, _lastname) {
    let logObj = {
        function: "parseConfirmationFlightHotelWithIdHotel"
        , params: _params
        , success: true
        , level: 'info'
    };

    let global_merge_vars = yield parseGlobalMergeVars(_params);

    let templateConfirmationScript = fs.readFileSync('views/templates/confirmationWithIds.handlebars', 'utf8');
    let templateVoucherHotelScript = fs.readFileSync('views/templates/voucher.handlebars', 'utf8');

    // Category image
    handlebars.registerHelper('hotelCatImg', function () {
        let hotelRating = parseInt(_params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].hotelRating);
        return getHotelRatingImageTag(hotelRating);
    });

    // capitalize text
    handlebars.registerHelper('capitalize', function (input) {
        if (!_.isString(input)) {
            return input;
        }
        let output = input.substring(0, 1).toUpperCase() + input.substring(1);
        return output.replace(/ *\([^)]*\) */g, "");
    });

    // upper text
    handlebars.registerHelper('upper', function (input) {
        if (!_.isString(input)) {
            return input;
        }
        let output = input.toUpperCase();
        return output.replace(/ *\([^)]*\) */g, "");
    });
    // format currency
    handlebars.registerHelper('priceFilter', function (val, currency) {
        let currencySymbol;
        if (currency === 'USD') {
            currencySymbol = 'US$';
        }
        else if (currency === 'CLP') {
            currencySymbol = '$';
        }
        else {
            currencySymbol = currency;
        }
        if (!isNaN(val)) {
            return currencySymbol + Math.ceil(val).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&.');
        }
        else {
            return val;
        }
    });

    let templateVoucherHotelHtml = handlebars.compile("{{#content}}" + templateVoucherHotelScript + "{{/content}}");
    let compiledVoucherHotelHtml = templateVoucherHotelHtml(global_merge_vars);

    let templateConfirmationHtml = handlebars.compile("{{#content}}" + templateConfirmationScript + "{{/content}}", { noEscape: true }, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        }
    });

    let compiledConfirmationHtml = templateConfirmationHtml(global_merge_vars, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        };
    });

    //// Generar pdf
    let pdfHotel = yield pdfGeneratorService.generatePdfFromStringToBuffer(compiledVoucherHotelHtml);

    let urlVoucher = "https://wapp.cocha.com/vouchers/:pnr/:lastname".replace(":pnr", _params.flight.itinerary.pnr).replace(":lastname", _lastname)
    let pdfVuelo = yield pdfGeneratorService.generatePdfFromUrlToStream(urlVoucher);

    let attachmentsPDF = [{
        type: "application/pdf",
        name: "Itinerario de vuelo.pdf",
        content: pdfVuelo
    },{
        type: "application/pdf",
        name: "Voucher de hotel.pdf",
        content: pdfHotel
    }];

    let config = {
        html: compiledConfirmationHtml,
        subject: "Gracias por tu compra en Cocha.com",
        from_email: "agenteinternet@cocha.com",
        from_name: "Cocha",
        signing_domain: "cocha.com",
        to: [{
                email: _email || _params.hotel.itinerary.itinerary.customer.email,
                name: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].firstName + ' ' + _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].lastName,
                type: "to"
        }],
        important: true,
        inline_css: true,
        attachments: attachmentsPDF,
    };
    config.to = addAdditionalBccEmails(config.to);
    return config;

};

function* parseSolicitudePayment(_params) {
    let logObj = {
        function: "parseSolicitudePayment"
        , params: _params
        , success: true
        , level: 'info'
    };

    handlebars.registerHelper('contactNames', function () {
        return _params.contact.names;
    });
    handlebars.registerHelper('contactPhone', function () {
        return _params.contact.phone;
    });
    handlebars.registerHelper('contactEmail', function () {
        return _params.contact.email;
    });
    handlebars.registerHelper('spnr', function () {
        return _params.spnr;
    });

    let templateHtmlScript = fs.readFileSync('views/templates/solicitude_payment.handlebars', 'utf8');
    let templateCompiledHtml = handlebars.compile(templateHtmlScript, { noEscape: true }, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        }
    });

    let compiledHtml = templateCompiledHtml();

    let config = {
        html: compiledHtml,
        subject: "Solicitud de compra en Cocha.com",
        from_email: "agenteinternet@cocha.com",
        from_name: "Cocha",
        signing_domain: "cocha.com",
        to: [
            {
                email: _params.contact.email,
                name: _params.contact.names,
                type: "to"
            }, {
                email: "agenteinternet@cocha.com",
                type: "bcc"
            }, {
                email: "fgargiulo@cocha.com",
                type: "bcc"
            }
        ],
        important: true,
        inline_css: true
    };

    return config;

};

function* parseDeferredPayment(_params, _params2, _email) {
    let logObj = {
        function: "parseConfirmationFlightHotel"
        , params: _params
        , success: true
        , level: 'info'
    };

    handlebars.registerHelper('contactFullName', function () {
        return _params.contactInfo.nombres + ' ' + _params.contactInfo.apellidos
    });

    handlebars.registerHelper('contactPhone', function () {
        return _params.contactInfo.phoneNumber;
    });

    handlebars.registerHelper('contactEmail', function () {
        return _params.contactInfo.email;
    });
    //hotelArrivalDate
    handlebars.registerHelper('hotelArrivalDate', function () {
        return moment(_params.hotel.details.arrivalDate).format("LL");
    });

    //hotelDepartureDate
    handlebars.registerHelper('hotelDepartureDate', function () {
        return moment(_params.hotel.details.departureDate).format("LL");
    });

    // expireDateTime
    handlebars.registerHelper('expireDateTime', function () {
        return moment(_params2.expireDateTime).format('lll');
    });

    // amount
    handlebars.registerHelper('amount', function () {
        return "CLP $ " + Math.ceil(_params2.amount).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&.');
    });

    // spnr
    handlebars.registerHelper('spnr', function () {
        return _params2.spnr.toUpperCase();
    });

    // Category image
    handlebars.registerHelper('hotelCatImg', function () {
        let hotelRating = parseInt(_params.hotel.details.starRating);
        return getHotelRatingImageTag(hotelRating);
    });

    // capitalize text
    handlebars.registerHelper('capitalize', function (input) {
        if (!_.isString(input)) {
            return input;
        }
        let output = input.substring(0, 1).toUpperCase() + input.substring(1);
        return output.replace(/ *\([^)]*\) */g, "");
    });

    // upper text
    handlebars.registerHelper('upper', function (input) {
        if (!_.isString(input)) {
            return input;
        }
        let output = input.toUpperCase();
        return output.replace(/ *\([^)]*\) */g, "");
    });

    //departureLongDate
    handlebars.registerHelper("departureLongDate", function () {
        let _flightDep = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "O") { return obj; } });
        let _flight = _.first(_flightDep);
        return moment(_flight.departureDateTime).format("ddd DD MMMM")
    })

    //returnLongDate
    handlebars.registerHelper("returnLongDate", function () {
        let _flightRet = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "R") { return obj; } });
        let _flight = _.first(_flightRet);
        return moment(_flight.departureDateTime).format("ddd DD MMMM")
    })

    //departureAirlineName
    handlebars.registerHelper("departureAirlineName", function () {
        let _flightDep = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "O") { return obj; } });
        let _flight = _.first(_flightDep);
        return _flight.airlineName
    })

    //returnAirlineName
    handlebars.registerHelper("returnAirlineName", function () {
        let _flightRet = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "R") { return obj; } });
        let _flight = _.first(_flightRet);
        return _flight.airlineName
    })

    //departureCabinType
    handlebars.registerHelper("departureCabinType", function () {
        let _flightDep = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "O") { return obj; } });
        let _flight = _.first(_flightDep);
        return _flight.cabinType
    })

    //returnCabinType
    handlebars.registerHelper("returnCabinType", function () {
        let _flightRet = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "R") { return obj; } });
        let _flight = _.first(_flightRet);
        return _flight.cabinType
    })

    //departureElapsedTime
    handlebars.registerHelper("departureElapsedTime", function () {
        let _flightDep = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "O") { return obj; } });
        let _flight = _.first(_flightDep);
        return _flight.elapsedTime
    })

    //returnElapsedTime
    handlebars.registerHelper("returnElapsedTime", function () {
        let _flightRet = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "R") { return obj; } });
        let _flight = _.first(_flightRet);
        return _flight.elapsedTime
    })

    // flights departure
    handlebars.registerHelper('flightDepDep', function () {
        let _flightDep = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "O") { return obj; } });
        let _flight = _.first(_flightDep);

        return "<b>" + _flight.cityOrigin.cityCode + "</b> / " + _flight.airlineCode + _flight.airlineNumber + " / " + moment(_flight.departureDateTime).format('HH:SS') + " / " + moment(_flight.departureDateTime).format('DD MMMM');
    });

    handlebars.registerHelper('flightDepCon', function () {
        let _flight = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if ((obj.connType === "I") && obj.directionFlag === "O") { return obj; } });

        let _scr = "";
        _.each(_flight, function (_flight, i) {
            if (i >= 1) _scr = + "<br>"
            _scr += "<b>" + _flight.cityOrigin.cityCode + "</b> / " + _flight.airlineCode + _flight.airlineNumber + " / " + moment(_flight.departureDateTime).format('HH:SS') + " / " + moment(_flight.departureDateTime).format('DD MMMM');
        })
        return _scr;
    });

    handlebars.registerHelper('flightDepArr', function () {
        let _flightDep = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "O") { return obj; } });
        let _flight = _.last(_flightDep);

        return "<b>" + _flight.cityDestination.cityCode + "</b> / " + _flight.airlineCode + _flight.airlineNumber + " / " + moment(_flight.arrivalDateTime).format('HH:SS') + " / " + moment(_flight.arrivalDateTime).format('DD MMMM');
    });

    // flights return
    handlebars.registerHelper('flightRetDep', function () {
        let _flightRet = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "R") { return obj; } });
        let _flight = _.first(_flightRet);

        return "<b>" + _flight.cityOrigin.cityCode + "</b> / " + _flight.airlineCode + _flight.airlineNumber + " / " + moment(_flight.departureDateTime).format('HH:SS') + " / " + moment(_flight.departureDateTime).format('DD MMMM');
    });

    handlebars.registerHelper('flightRetCon', function () {
        let _flight = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if ((obj.connType === "I") && obj.directionFlag === "R") { return obj; } });

        let _scr = "";
        _.each(_flight, function (_flight, i) {
            if (i >= 1) _scr = + "<br>"
            _scr += "<b>" + _flight.cityOrigin.cityCode + "</b> / " + _flight.airlineCode + _flight.airlineNumber + " / " + moment(_flight.departureDateTime).format('HH:SS') + " / " + moment(_flight.departureDateTime).format('DD MMMM');
        })
        return _scr;
    });

    handlebars.registerHelper('flightRetArr', function () {
        let _flightRet = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "R") { return obj; } });
        let _flight = _.last(_flightRet);

        return "<b>" + _flight.cityDestination.cityCode + "</b> / " + _flight.airlineCode + _flight.airlineNumber + " / " + moment(_flight.arrivalDateTime).format('HH:SS') + " / " + moment(_flight.arrivalDateTime).format('DD MMMM');
    });

    //hotelName
    handlebars.registerHelper('hotelName', function () {
        return _params.hotel.details.name
    });

    //hotelAddress
    handlebars.registerHelper('hotelAddress', function () {
        return _params.hotel.details.address + ". " + _params.hotel.details.country
    });

    //number of pax
    handlebars.registerHelper('hotelPaxes', function () {
        let adt = _.filter(_params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO, { "passengerType": "ADT" }).length || 0
        let pfa = _.filter(_params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO, { "passengerType": "PFA" }).length || 0
        let chd = _.filter(_params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO, { "passengerType": "CHD" }).length || 0
        let cnn = _.filter(_params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO, { "passengerType": "CNN" }).length || 0
        let nights = moment(_params.hotel.details.departureDate).diff(_params.hotel.details.arrivalDate) / 86400000
        return (adt + pfa) + " Adultos | " + (chd + cnn) + " Niños | " + parseInt(nights) + " Noches"
    });

    //hotelNotes
    handlebars.registerHelper('hotelNotes', function () {
        //notas del hotel
        let config_hotel_notes = _params.hotel.itinerary.itinerary.hotelConfirmation[0].valueAdds || [];
        let notes;
        _.each(_config_hotelNotes.valueAdd, function (_config_hotelNotes) {
            notes += _config_hotelNotes.description + ". ";
        });
        return notes;
    });

    //checkInTime
    handlebars.registerHelper('checkInTime', function () {
        if (_params.hotel.details.checkInTime.toLowerCase() === "noon") {
            return moment(_params.hotel.details.arrivalDate).format("dddd DD MMMM") + ", desde las 12 AM"
        } else {
            if (_params.hotel.details.checkInTime === "") {
                return moment(_params.hotel.details.arrivalDate).format("dddd DD MMMM")
            } else {
                return moment(_params.hotel.details.arrivalDate).format("dddd DD MMMM") + ", desde las " + _params.hotel.details.checkInTime
            }
        }
    });

    //checkOutTime
    handlebars.registerHelper('checkOutTime', function () {
        if (_params.hotel.details.checkOutTime.toLowerCase() === "noon") {
            return moment(_params.hotel.details.departureDate).format("dddd DD MMMM") + ", hasta las 12 AM"
        } else {
            if (_params.hotel.details.checkOutTime === "") {
                return moment(_params.hotel.details.departureDate).format("dddd DD MMMM")
            } else {
                return moment(_params.hotel.details.departureDate).format("dddd DD MMMM") + ", hasta las " + _params.hotel.details.checkOutTime
            }
        }
    });

    //expire
    handlebars.registerHelper('expire', function () {
        return moment(_params.flight.itinerary.bookingDate).format()
    });

    //departureArrivalAirportName
    handlebars.registerHelper('departureArrivalAirportName', function () {
        let _flightDep = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "O") { return obj; } });
        let _flight = _.last(_flightDep);
        return _flight.arrivalTerminal
    });

    //returnDepartureAirportName
    handlebars.registerHelper('returnDepartureAirportName', function () {
        let _flightRet = _.filter(_params.flight.itinerary.bookingFlightInfoList.bookingFlightInfoDTO, function (obj) { if (obj.directionFlag === "R") { return obj; } });
        let _flight = _.first(_flightRet);
        return _flight.arrivalTerminal
    });

    // format currency
    handlebars.registerHelper('priceFilter', function (val, currency) {
        let currencySymbol;
        if (currency === 'USD') {
            currencySymbol = 'US$';
        }
        else if (currency === 'CLP') {
            currencySymbol = '$';
        }
        else {
            currencySymbol = currency;
        }
        if (!isNaN(val)) {
            return currencySymbol + Math.ceil(val).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&.');
        }
        else {
            return val;
        }
    });

    let templateHtmlScript = fs.readFileSync('views/templates/deferred_payment.handlebars', 'utf8');
    let templateCompiledHtml = handlebars.compile(templateHtmlScript, { noEscape: true }, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        }
    });

    let flags = {
        transfer: (_params2.transfer === "yes") ? true : false,
        assistance: (_params2.assistance === "yes") ? true : false
    }

    let compiledHtml = templateCompiledHtml(flags);

    // console.log(compiledHtml);

    let config = {
        html: compiledHtml,
        subject: "Gracias por cotizar en Cocha.com",
        from_email: "agenteinternet@cocha.com",
        from_name: "Cocha",
        signing_domain: "cocha.com",
        to: [
            {
                email: _params2.email || _params.flight.itinerary.contactEmail,
                name: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].firstName + ' ' + _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].lastName,
                type: "to"
            }, {
                email: "agenteinternet@cocha.com",
                type: "bcc"
            }, {
                email: "fgargiulo@cocha.com",
                type: "bcc"
            }
        ],
        important: true,
        inline_css: true
    };

    return config;

};

function* parseOportunity(_params, _pnr) {

    handlebars.registerHelper('contactNames', function () {
        return _params.contact.names;
    });
    handlebars.registerHelper('contactPhone', function () {
        return _params.contact.phone;
    });
    handlebars.registerHelper('contactEmail', function () {
        return _params.contact.email;
    });
    handlebars.registerHelper('spnr', function () {
        return _params._pnr;
    });

    let templateHtmlScript = fs.readFileSync('views/templates/solicitude_payment.handlebars', 'utf8');
    let templateCompiledHtml = handlebars.compile(templateHtmlScript, { noEscape: true }, function (err, res) {
        if (err) {
            Koa.log.error(err);
        } else {
            return res;
        }
    });

    let compiledHtml = templateCompiledHtml();
    let buffTemp = Buffer.from(JSON.stringify(_params, null, 2));

    return yield {
        "html": compiledHtml,
        "subject": "Intento de pago en Cocha.com",
        "from_email": "agenteinternet@cocha.com",
        "from_name": "Cocha.com",
        "to": [{
            "email": "intenciones-pago-vh@cocha.com",
            "name": "Cocha.com",
            "type": "to"
        }],
        "important": true,
        "merge": true,
        "merge_language": "handlebars",
        "global_merge_vars": [{
            "name": "params",
            "content": [_params]
        },
        {
            "name": "amount",
            "content": _params.info.detailPrice.totalPrice.usd
        }]
        , "attachments": [{
            "type": "text/plain; charset=UTF-8",
            "name": "log.txt",
            "content": buffTemp.toString('base64')
        }]
    };
};

function* parseOportunityOld(_params, _pnr) {
    let pnrHtml = '';
    if (_pnr) {
        _params.pnr = _pnr;
        pnrHtml = '<h4> SPNR: <b>{{pnr}}</b></h4>';
    }

    let buffTemp = Buffer.from(JSON.stringify(_params, null, 2));

    let config = {
        "html": "{{#params}}<h2>Se registro un intento de pago:</h2><h3>Detalles</h3>" + pnrHtml + "<h4> Email: <b>{{contact.email}}</b></h4><h4> Monto: <b>{{../amount}}</b></h4><h4> Destino: <b>{{destination}}</b></h4><h4> Ida: <b>{{arrival}}</b></h4><h4> Vuelta: <b>{{departure}}</b></h4><h4> Método de pago: <b>{{paymentMethod}}</b></h4><h4> Hotel ID: <b>{{100371625}}</b></h4><h4>Habitaciones</h4> {{#rooms}}<ul><li>Tipo de cama: {{bedType}}</li><li>Adultos: {{adultCount}}</li><li>Niños:<ul> {{#childrenAges}}<li>Edad: {{age}}</li>{{/childrenAges}}</ul></li></ul> {{/rooms}}<h2> <b>{{#businessData}}{{authAmount}}{{/businessData}}</b></h2><h4>Contacto</h4><ul><li><b>email:</b> {{contact.email}}</li><li><b>phone:</b> {{contact.phone}}</li><li><b>names:</b> {{contact.names}}</li><li><b>lastNames:</b> {{contact.lastnames}}</li></ul><h4>Pasajeros</h4> {{#passengers}}<ul><li><b>firstName: </b>{{firstName}}</li><li><b>firstLastName: </b>{{firstLastName}}</li><li><b>secondLastName: </b>{{secondLastName}}</li><li><b>type: </b>{{type}}</li><li><b>gender: </b>{{gender}}</li><li><b>documentNumber: </b>{{documentNumber}}</li><li><b>documentType: </b>{{documentType}}</li><li><b>documentCountry: </b>{{documentCountry}}</li><li><b>birthDate: </b>{{birthDate}}</li></ul> {{/passengers}}{{/params}}",
        "subject": "Intento de pago",
        "from_email": "agenteinternet@cocha.com",
        "from_name": "Cocha.com",
        "to": [{
            "email": "intenciones-pago-vh@cocha.com",
            "name": "Cocha.com",
            "type": "to"
        }],
        "important": true,
        "merge": true,
        "merge_language": "handlebars",
        "global_merge_vars": [{
            "name": "params",
            "content": [_params]
        },
        {
            "name": "amount",
            "content": _params.info.detailPrice.totalPrice.usd
        }]
        , "attachments": [{
            "type": "text/plain; charset=UTF-8",
            "name": "log.txt",
            "content": buffTemp.toString('base64')
        }]
    };

    return config;
};

function* parsePaymentOportunity(_emailTo, _emailBcc) {

    let oportunityFlag = yield oportunityModel.existsOportunity(_emailTo);

    if (oportunityFlag) {
        let oportunity = yield oportunityModel.getOportunity(_emailTo);

        // Category image
        handlebars.registerHelper('hotelCatImg', function () {
            let hotelRating = parseInt(oportunity.info.hotel.stars);
            return getHotelRatingImageTag(hotelRating);
        });
        // capitalize text
        handlebars.registerHelper('capitalize', function (input) {
            if (!_.isString(input)) {
                return input;
            }
            let output = input.substring(0, 1).toUpperCase() + input.substring(1);
            return output.replace(/ *\([^)]*\) */g, "");
        });
        // upper text
        handlebars.registerHelper('upper', function (input) {
            if (!_.isString(input)) {
                return input;
            }
            let output = input.toUpperCase();
            return output.replace(/ *\([^)]*\) */g, "");
        });
        // format currency
        handlebars.registerHelper('priceFilter', function (val, currency) {
            let currencySymbol;
            if (currency === 'USD') {
                currencySymbol = 'US$';
            }
            else if (currency === 'CLP') {
                currencySymbol = '$';
            }
            else {
                currencySymbol = currency;
            }
            if (!isNaN(val)) {
                return currencySymbol + Math.ceil(val).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&.');
            }
            else {
                return val;
            }
        });


        let global_merge_vars = {
            name: "contexto",
            content: [
                {
                    codReservations: [],
                    paymentInfo: [
                        {
                            fullName: oportunity.contact.names + " " + oportunity.contact.lastNames,
                            rut: oportunity.passengers[0].documentNumber,
                            telephone: oportunity.contact.phone,
                            email: oportunity.contact.email,
                            amounts: [
                                {
                                    hotelUSD: parseInt(oportunity.info.detailPrice.mainBase),
                                    tasasCargosUSD: parseInt(oportunity.info.detailPrice.tax) + parseInt(oportunity.info.detailPrice.fee),
                                    feeUSD: oportunity.info.detailPrice.fee,
                                    transferUSD: oportunity.info.detailPrice.transfer.usd,
                                    asistenciaUSD: oportunity.info.detailPrice.assistancePrice.usd,
                                    totalUSD: parseInt(oportunity.info.detailPrice.totalPrice.usd),
                                    totalCLP: oportunity.info.detailPrice.totalPrice.clp,
                                }
                            ],
                        }
                    ],
                    itineraryHotel: [
                        {
                            arrivalDate: moment(oportunity.arrival).format("dddd DD MMMM"),
                            departureDate: moment(oportunity.departure).format("dddd DD MMMM"),
                            adultCount: oportunity.info.pax.adults || 0,
                            childCount: oportunity.info.pax.childrens || 0,
                            nights: oportunity.info.hotel.night || 0,
                            name: oportunity.info.hotel.name,
                            address: oportunity.info.address,
                            cityName: oportunity.destination,
                            hotelRating: oportunity.info.hotel.stars,
                            policyDescription: oportunity.info.policies.flight,
                            cancellationPolicy: oportunity.info.policies.hotel,
                        }
                    ],
                    itineraryFlight: [
                        {
                            bookingFlights: [
                                {
                                    flightDeparture: moment(oportunity.arrival).format("dddd DD MMMM"),
                                    flightReturn: moment(oportunity.departure).format("dddd DD MMMM")
                                }
                            ]
                        }
                    ],
                    flagAssistance: (oportunity.info.detailPrice.assistancePrice.usd > 0),
                    flagTransfer: (oportunity.info.detailPrice.transfer.usd > 0),
                    itineraryTransfer: {
                        idTransfers: 7,
                        mainPassenger: oportunity.contact.names + " " + oportunity.contact.lastNames,
                        origin: "Aeropuerto",
                        destination: oportunity.destination,
                        hotel: oportunity.info.hotel.name,
                        airport: "Aeropuerto",
                        ianCode: "935",
                        provider: "NEXUS",
                        transfersType: "Regular",
                        baggageIncluded: "1 Maleta de 23 kg y 1 bolso de mano",
                        phoneContact: "Nexus Center +5219982831900",
                        locationTransfers: "El conductor te estar&aacute; esperando en el sector de<br> llegada de vuelos con un cartel con tu nombre.",
                        cancellationPolicy: "Sin costo hasta 72 hrs. antes de la llegada del vuelo",
                        requiredDocuments: "Voucher de Servicios",
                        babyChairs: "Producto disponible con solicitud previa",
                    }
                },
            ]
        };

        let templateOportunityScript = fs.readFileSync('views/templates/oportunity.handlebars', 'utf8');

        let templateOportunityHtml = handlebars.compile("{{#content}}" + templateOportunityScript + "{{/content}}", { noEscape: true });

        let compiledOportunityHtml = templateOportunityHtml(global_merge_vars);

        let config = {
            "html": compiledOportunityHtml,
            "subject": "Tu cotización en Cocha.com",
            "from_email": "agenteinternet@cocha.com",
            "from_name": "Cocha.com",
            "to": [{
                "email": _emailBcc || _emailTo,
                "name": "Cocha.com",
                "type": "to"
            }],
            "important": true,
        };


        return config;
    } else {
        return false;
    }
};

function parseGoogleSchemaHotel(_params) {
    let hotelSchema = {
        "@context": "http://schema.org",
        "@type": "",
        "reservationNumber": _params.hotel.itinerary.itinerary.itineraryId,
        "reservationStatus": "http://schema.org/Confirmed",
        "underName": {
            "@type": "Person",
            "name": _params.hotel.itinerary.itinerary.customer.firstName + ' ' + _params.hotel.itinerary.itinerary.customer.lastName
        },
        "reservationFor": {
            "@type": "LodgingBusiness",
            "name": _params.hotel.itinerary.itinerary.hotelConfirmation[0].confirmationNumber,
            "address": {
                "@type": "PostalAddress",
                "streetAddress": _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].address1,
                "addressLocality": _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].city,
                "addressRegion": _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].stateProvinceCode,
                "postalCode": _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].postalCode,
                "addressCountry": _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].countryCode
            },
            "telephone": _params.hotel.itinerary.itinerary.hotelConfirmation[0].hotel[0].phone
        },
        "checkinDate": _params.hotel.itinerary.itinerary.hotelConfirmation[0].arrivalDate,
        "checkoutDate": _params.hotel.itinerary.itinerary.hotelConfirmation[0].departureDate
    };

    return hotelSchema;
}

function parseGoogleSchemaFlight(_params) {
    let flightSchema = {
        "@context": "http://schema.org",
        "@type": "FlightReservation",
        "reservationNumber": "RXJ34P",
        "reservationStatus": "http://schema.org/Confirmed",
        "underName": {
            "@type": "Person",
            "name": "Eva Green"
        },
        "reservationFor": {
            "@type": "Flight",
            "flightNumber": "110",
            "airline": {
                "@type": "Airline",
                "name": "United",
                "iataCode": "UA"
            },
            "departureAirport": {
                "@type": "Airport",
                "name": "San Francisco Airport",
                "iataCode": "SFO"
            },
            "departureTime": "2017-04-15T20:15:00-08:00",
            "arrivalAirport": {
                "@type": "Airport",
                "name": "John F. Kennedy International Airport",
                "iataCode": "JFK"
            },
            "arrivalTime": "2017-04-16T06:30:00-05:00"
        }
    };

    return flightSchema;
}

function* sendAlertEmailNotSent(_params, _msg) {

    let buffTemp = Buffer.from(JSON.stringify(_params, null, 2));

    if (_.has(_params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0], "rph")) {
        let contactInfo = {
            firstName: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].firstName,
            lastName: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].lastName,
            rut: _params.flight.itinerary.bookingPaxInfoList.bookingPaxInfoDTO[0].docNumber,
        }
    }

    if (_.has(_params.flight.itinerary, "pnr")) {
        let flightItineraryInfo = {
            fecha_vuelo: _params.flight.itinerary.travelDate,
            codigo: _params.flight.itinerary.destPost.cityCode,
            ciudad: _params.flight.itinerary.destPost.cityName,
            pais: _params.flight.itinerary.destPost.countryName,
            fecha_limite_ticket: moment(_params.flight.itinerary.ticketTimeLimit).format("dddd DD MMMM HH:SS")
        }
    }

    if (_.has(_params.hotel.details, "id")) {
        let hotelDetailInfo = {
            id_hotel: _params.hotel.details.id,
            nombre: _params.hotel.details.name,
            direccion: _params.hotel.details.address
        }
    }

    if (_.has(_params.hotel.itinerary, "itineraryId")) {
        let hotelItineraryInfo = {
            checkin: moment(_params.hotel.itinerary.itinerary.hotelConfirmation[0].arrivalDate).format("dddd DD MMMM"),
            checkOut: moment(_params.hotel.itinerary.itinerary.hotelConfirmation[0].departureDate).format("dddd DD MMMM"),
            adultos: _params.hotel.itinerary.itinerary.hotelConfirmation[0].numberOfAdults,
            niños: _params.hotel.itinerary.itinerary.hotelConfirmation[0].numberOfChildren,
            noches: _params.hotel.itinerary.itinerary.hotelConfirmation[0].nights,
        }
    }

    let config = {
        "html": "<h1 style=\"color:red\">Voucher no enviado: *|pnr|*</h1><h2>No se ha envíado el voucher de confirmación de compra V+H al siguiente destinatario:</h2><ul><li>*|emailTo|*</li></ul><h2>El motivo podría resultar el siguiente:</h2><p> <b>*|errMsg|*</b></p><h3>Detalles</h3><h4>Contacto</h4><p>*|contactInfo|*</p><h4>Itinerario de vuelo</h4><p>*|flightItineraryInfo|*</p><h4>Itinerario del hotel</h4><p>*|hotelItineraryInfo|*</p><h4>Detalle del Hotel</h4><p>*|hotelDetailInfo|*</p>",
        "subject": "Error al enviar voucher",
        "from_email": "agenteinternet@cocha.com",
        "from_name": "Cocha.com",
        "to": [{
            "email": "jmpaz@cocha.com",
            "name": "Cocha.com",
            "type": "to"
        }, {
            "email": "fgargiulo@cocha.com",
            "name": "Cocha.com",
            "type": "bcc"
        }],
        "important": true,
        "merge": true,
        "merge_language": "mailchimp",
        "global_merge_vars": [{
            "name": "pnr",
            "content": _params.payment.storeCode
        }, {
            "name": "emailTo",
            "content": _params.payment.paymentEmail
        }, {
            "name": "contactInfo",
            "content": JSON.stringify(contactInfo, null, 2) || "Sin Datos"
        }, {
            "name": "flightItineraryInfo",
            "content": JSON.stringify(flightItineraryInfo, null, 2) || "Sin Datos"
        }, {
            "name": "hotelItineraryInfo",
            "content": JSON.stringify(hotelItineraryInfo, null, 2) || "Sin Datos"
        }, {
            "name": "hotelDetailInfo",
            "content": JSON.stringify(hotelDetailInfo, null, 2) || "Sin Datos"
        }, {
            "name": "errMsg",
            "content": _msg
        }, {
            "name": "errLog",
            "content": JSON.stringify(_params)
        }]
        , "attachments": [{
            "type": "text/plain; charset=UTF-8",
            "name": "log.txt",
            "content": buffTemp.toString('base64')
        }]
    };
    return yield new Promise((resolve, reject) => {
        sendMailService.sendMail(config, (err, result) => {
            if (err) {
                Koa.log.error(err);
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

/**
 * Utility function that, given a integer, return the star-rating image tag.
 * @param {int} _starRating
 * @returns {string} The HTML tag with the star-rating image
 */
function getHotelRatingImageTag(_starRating) {
    _starRating = parseInt(_starRating) || 0;
    const galleryBaseUrl = 'https://gallery.mailchimp.com/cf8e3f784ee65fee4bea0e12f/images/';
    const images = [
        'afa46f93-1ed9-4030-8254-441e4c1ef644.png', // I'm going to set the one start when rating == 0
        'afa46f93-1ed9-4030-8254-441e4c1ef644.png',
        '32d372e0-a063-479d-a042-a3a0b1ed882b.png',
        '3856a52c-be80-4e4d-b2e4-d3dfe5a2f6a0.png',
        'b719cc0c-f057-41ca-8240-5e9852233ddd.png',
        'c3ab6644-08e5-48f4-9551-02e8aa84ab8f.png',
    ];
    return `<img src="${galleryBaseUrl}${images[_starRating]}">`;
}

module.exports = {
    parseConfirmationFlightHotel: parseConfirmationFlightHotel,
    parsePdfVoucher: parsePdfVoucher,
    parseOportunity: parseOportunity,
    parseOportunityOld: parseOportunityOld,
    parsePaymentOportunity: parsePaymentOportunity,
    parseDeferredPayment: parseDeferredPayment,
    parseSolicitudePayment: parseSolicitudePayment,
    parseConfirmationFlightHotelWithIds: parseConfirmationFlightHotelWithIds
}
