/**
 * FlightPlusHotelService.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let externalServices 		= require('cocha-external-services');
let flightHotelServices 	= externalServices.flightHotelServices;
let flightServices 			= externalServices.flightsServices;
let hotelServices 			= externalServices.hotelServices;
let crmServices 			= externalServices.crmServices;
let webServices 			= externalServices.webServices;
let sendMailService 		= externalServices.sendMail;
let flightParser   			= require('./FlightParser');
let hotelParser    			= require('./HotelParser');
let flightPlusHotelParser   = require('./FlightPlusHotelParser');
let mailParser 				= require('./MailingParser');
let deltaModel				= require('../models/redis/Delta');
let blacklistModel			= require('../models/redis/Blacklist');
let rateRulesModel			= require('../models/redis/RateRules');
let bookingLogModel			= require('../models/mysql/BookingLog');
let oportunityModel 		= require('../models/redis/Oportunity');
let externalRequest 		= require('request');


function* autocomplete(_params){
	return yield new Promise((resolve, reject) => {
		flightHotelServices.autocomplete(_params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
}

function* getFlightsWithHotels(_params, _workflowId) {
	let resultParsed = yield {
		flights: function*() {
			// let flights = yield new Promise((resolve, reject) => {
			// 	flightServices.search(_params.flights, (err, result) => {
			// 		if (err) {
			// 			Koa.log.error(err);
			// 			reject(err);
			// 		} else {
			// 			resolve(result);
			// 		}
			// 	}, _workflowId);
			// });

			// let flightsParsed =  yield flightParser.parseFlights(flights, _params.flights.numADT,  _params.flights.numCHD, _params.flights.numINF, _params.flights.channelToFpH);

			let flightsParsed = yield new Promise((resolve, reject) => {
				flightServices.search(_params.flights, (err, result) => {
					if (err) {
						Koa.log.error(err);
			            reject({
			              message: {
			                msg: err.msg || err,
			                code: (!err.name && err === 'flights not found') ? 'noFlightsFound': err.name
			              },
			              data: err
			            });
					} else {
						resolve(result);
					}
				}, _workflowId);
			});

			Koa.log.infoSL({
				action: 'SearchFlights',
				searchParams: _params.flights,
				priceData: flightsParsed.dataPrices
			}, _workflowId);
			delete flightsParsed.dataPrices;

			deltaModel.setDelta(flightsParsed.referencePrice.id, flightsParsed.referencePrice);
			return flightsParsed;
		},
		transfer: function*() {
			return yield getTransfer(_params.transfers, _workflowId);
		},
		hotelsJust: function*() {
			let hotels = yield searchHotels(_params.hotels.just, _workflowId);
			return yield hotelParser.parseHotels(hotels, _params.hotels.just);
		},
		hotelsLate: function*() {
			//return yield searchHotels(_params.hotels.late, _workflowId);
			let hotels = yield searchHotels(_params.hotels.late, _workflowId);
			if(hotels){
			  return yield hotelParser.parseHotels(hotels, _params.hotels.late);
			} else {
			  return hotels;
			}
		}
	};
	let flightPlusHotelsParsed = yield flightPlusHotelParser.parseFlightsPlusHotels(_params, resultParsed);
	return flightPlusHotelsParsed;
	// return resultParsed.flights;
}

function* pricingFlightsWithHotels(_params, _workflowId) {
	let pricingParsed;
	let isKuality = (_params.hotels.supplier === 'TRV') ? true : false ;
	try {
		pricingParsed = yield {
			pricingFlights: new Promise((resolve, reject) => {
				flightServices.pricing(_params.flights, (err, result) => {
					if (err) {
						Koa.log.error(err);
						reject(err);
					} else {
						resolve(result);
					}
				}, _workflowId);
			}),
			pricingHotels: new Promise((resolve, reject) => {
				if(isKuality) {
					hotelServices.checkoutInformation(_params.hotels, (err, result) => {
						if (err) {
							Koa.log.error(err);
							reject(err);
						} else {
							resolve(flightPlusHotelParser.parseCheckoutInformation(result,_params.hotels));
						}
					}, _workflowId);
				} else {
					hotelServices.getMetaAvailability(_params.hotels, (err, result) => {
						if (err) {
							Koa.log.error(err);
							reject(err);
						} else {
							resolve(result);
						}
					}, _workflowId);
				}
			})
			,
			hotelDetails: new Promise((resolve, reject) => {	
				hotelServices.hotelInfo({hotelId:_params.hotels.hotelID}, (err, result) => {
					if (err) {
						Koa.log.error(err);
						reject(err);
					} else {
						resolve(result);
					}
				}, _workflowId);
			}),

			transfer: function*() {
				return yield getTransfer(_params.transfers, _workflowId);
			}
		};
	} catch (err) {
		let catchErr = {
			message: {
				msg: err.msg,
				code: 'InternalError'
			},
			data: err
		};
		if (err.name === 'PricingError') {
			catchErr.status = 400;
			catchErr.message.code = 'ProccessError';
		} else if (err.name === 'AvailabilityError') {
			catchErr.status = 400;
			catchErr.message.code = 'RoomError';
		}
		throw catchErr;
	}

	let flightPlusHotelsParsed = yield flightPlusHotelParser.parsePricingFlightsPlusHotels(_params, pricingParsed.pricingFlights, pricingParsed.pricingHotels, pricingParsed.transfer, pricingParsed.hotelDetails);

	Koa.log.infoSL({
		action: 'PricingFlightsHotels',
		searchParams: _params,
		pricingData: {
			hotelId: pricingParsed.pricingHotels.hotelIdSearch ,
			hotelName: pricingParsed.pricingHotels.hotelName,
			roomRateCode: pricingParsed.pricingHotels.hotelRoomResponse.rateCode,
			roomTypeCode: pricingParsed.pricingHotels.hotelRoomResponse.roomTypeCode,
			roomName: pricingParsed.pricingHotels.hotelRoomResponse.rateDescription,
			hotelCustomerSessionId: pricingParsed.pricingHotels.customerSessionId,
			flightSecurityToken: pricingParsed.pricingFlights.securityToken
		},
		priceData: flightPlusHotelsParsed.dataPrices
	}, _workflowId);
	delete flightPlusHotelsParsed.dataPrices;

	return flightPlusHotelsParsed;
}

function* quoteFlightsWithHotels(_params, _workflowId) {
	let flightPlusHotelsParsed = yield pricingFlightsWithHotels(_params, _workflowId);

	let oldPrice = _params.price;
	let currentPrice = flightPlusHotelsParsed.prices.clp.total + flightPlusHotelsParsed.prices.clp.localTax;

	if (oldPrice && currentPrice && (currentPrice <= oldPrice * 1.15) && currentPrice <= oldPrice + 70000) {
		let quoteParsed = yield flightPlusHotelParser.parseQuotation(_params, flightPlusHotelsParsed); //Parse data for CRM
		let crmResponse = yield new Promise((resolve, reject) => {
			crmServices.addQuote(quoteParsed, (err, result) => {
				if (err) {
					Koa.log.error(err);
					let catchErr = {
						status: 400,
						message: {
							msg: JSON.stringify(err),
							code: 'CrmError'
						}
					};
					reject(catchErr);
				} else {
					resolve(result);
				}
			}, _workflowId);
		});

		Koa.log.infoSL({
			action: 'CrmQuoteFlightsHotels',
			searchParams: quoteParsed,
			data: crmResponse
		}, _workflowId);

		return {
			statusCrm: true,
			msgCrm: JSON.stringify(crmResponse),
			prices: flightPlusHotelsParsed.prices
		};
	} else {
		let catchErr = {
			status: 400,
			message: {
				msg: 'La tarifa ha cambiado ' + oldPrice + ' --> ' + currentPrice,
				code: 'PriceError'
			}
		};
		throw catchErr;
	}
}

function* getHotelDetail(_params, _workflowId) {
	let result = {};
	let hotelParsed;
	let params = {};
	var delta = yield deltaModel.getDelta(_params.searchId);
	if(_.has(_params, 'latitude') && _.has(_params, 'longitude')){
		params.latitude = _params.latitude;
		params.longitude = _params.longitude;
		result = yield {
			pointsInterest : getPointOfInterest(params, _workflowId),
			hotelDetail: getHotelDetailInfo(_params, _workflowId)
		};
	} else {
		result.hotelDetail = yield getHotelDetailInfo(_params, _workflowId);
		params.latitude = result.hotelDetail.info.latitude;
		params.longitude = result.hotelDetail.info.longitude;
		result.pointsInterest = yield getPointOfInterest(params, _workflowId);
	}

	hotelParsed = yield hotelParser.parseHotelDetail(result, _params, delta);

	Koa.log.infoSL({
		action: 'SearchHotelDetails',
		searchParams: _params,
		searchData: {
			hotelId: result.hotelDetail.info.id,
			hotelName: result.hotelDetail.info.name,
			regionData: result.hotelDetail.regionData
		},
		priceData: hotelParsed.dataPrices
	}, _workflowId);
	// delete hotelParsed.dataPrices;

	return hotelParsed.hotelDetail;
}

function* setBlacklist() {
	return yield new Promise((resolve, reject) => {
		webServices.returnUrl('flightshotels','blackList',function(err,result){
			if(err){
				reject(err);
			} else {
				webServices.get('flightshotels', result.url, {}, null, (err, result) => {
					if (err) {
						Koa.log.error(err);
						reject(err);
					} else {
						blacklistModel.setBlacklist(flightPlusHotelParser.parseBlacklist(result));
						resolve({result:'success'});
					}
				});
			}
		});
	});
}

function* setRateRules() {
	return yield new Promise((resolve, reject) => {
		webServices.returnUrl('flightshotels','rateRules',function(err,result){
			if(err){
				reject(err);
			} else {
				webServices.get('flightshotels', result.url, {}, null, (err, result) => {
					if (err) {
						Koa.log.error(err);
						reject(err);
					} else {
						rateRulesModel.setRateRules(flightPlusHotelParser.parseRateRules(result));
						resolve({result:'success'});
					}
				});
			}
		});
	});
}

function* trackBooking(_pnr) {
	let bookingStates = yield bookingLogModel.find({where: {spnr: _pnr}, fields: {id: false, request: false, response: true, currentDate: false, spnr: false, step: true, provider: false}});
	let resp;
	let error;
	let validSteps = {
		availabilityFlight: false,
		bookFlight: false,
		bookStep: false,
		availabilityHotel: false,
		getBusinessOne: false,
		payOnHost: false,
		bookHotel: false,
		sendMail: false,
		getBusinessTwo: false,
		sendSmartFlight: false,
		sendSmartHotel: false,
		bookEnd: false
	};

	_.each(bookingStates, (step, idx) => {
		switch (step.step) {
			case 'TARIFICACION_FLIGHT':
				validSteps.availabilityFlight = true;
				break;
			case 'RESERVA_FLIGHT':
				validSteps.bookFlight = true;
				break;
			case 'CHECKBOOK_FH':
				validSteps.bookStep = true;
				break;
			case 'AVAI_HOTEL':
				validSteps.availabilityHotel = true;
				break;
			case 'OBTENER_NEGOCIOF':
				if (validSteps.getBusinessOne) {
					validSteps.getBusinessTwo = true;
				} else {
					validSteps.getBusinessOne = true;
				}
				break;
			case 'HOST_TO_HOST_PAY':
			case 'CREDIT_LINE_PAY':
				validSteps.payOnHost = true;
				break;
			case 'BOOKING_HOTEL':
				validSteps.bookHotel = true;
				break;
			case 'SEND_MAIL':
				validSteps.sendMail = true;
				break;
			case 'SEND_SMART_FLIGHT':
				validSteps.sendSmartFlight = true;
				break;
			case 'SEND_SMART_HOTEL':
				validSteps.sendSmartHotel = true;
				break;
			case 'RESPONSE_BOOKING':
				let response = JSON.parse(step.response);
				if (_.get(response, 'description', null)) { // Este 'If' es para retrocompatibilidad, borrar una vez termine la migracion (Dejar tal 'if = true')
					if (!_.get(response, 'status', null) || !_.get(response, 'code', null) || !_.get(response, 'description', null)) {
						error = {
							msg: 'Bad formed response. Expected to have "status", "code" and "description" properties. Obtained: ' +  JSON.stringify(response),
							code: 'BridgeError',
							data: response
						};
					} else
					if (response.status === 'NOK' && response.description !== 'DIFERENCIA DE PRECIOS' && response.description !== 'RESERVA DUPLICADA') {
						if (response.description === 'ERROR ENVIO A SMART-HOTEL' || response.description === 'ERROR ENVIO A SMART-VUELO' ||
								response.description === 'ERROR AL OBTENER VALOR KUALITY' || response.description === 'ERROR AL PROCESAR LA RESERVA EN VUELO' ||
								response.description === 'TOKEN NO GENERADO' || response.description === 'PAGO HOST TO HOST INVALIDO') {
							error = {
								msg: 'Error booking. Data: ' + JSON.stringify(response),
								code: 'BookingError',
								data: response
							};
						} else
						if (response.description === 'ERROR EN SERVICIO DE DISPONIBILIDAD HOTEL') {
							error = {
								msg: 'Error on hotel availability. Data: ' + JSON.stringify(response),
								code: 'HotelError',
								data: response
							};
						} else
						if (response.description === 'ERROR EN SERVICIO DETALLES DE VUELOS') {
							error = {
								msg: 'Error in flight details. Data: ' + JSON.stringify(response),
								code: 'FlightError',
								data: response
							};
						} else
						if (response.description === 'ERROR AL ACTUALIZAR REGISTROS DE PRICE BOOKING' || response.description === 'ERROR AL ACTUALIZAR EPNR DE VUELOS' ||
								response.description === 'ERROR AL OBTENER EL OBJETO EANWSERROR') {
							error = {
								msg: 'Error on manage booking. Data: ' + JSON.stringify(response),
								code: 'BridgeError',
								data: response
							};
						} else
						if (response.description === 'NEGOCIO RECHAZADO') {
							error = {
								msg: 'Business number rejected. Data: ' + JSON.stringify(response),
								code: 'BusinessError' + ((response.code === 1)? '-fatal' : ''),
								data: response
							};
						} else
						if (response.description === 'NEGOCIO NO EXISTE EN SMART') {
							error = {
								msg: 'Business number unexist. Data: ' + JSON.stringify(result),
								code: 'UnexistBusinessError',
								data: response
							};
						} else
						if (response.description === 'TARJETA RECHAZADA' || response.description === 'LLAME A AUTORIZAR' || response.description === 'RETENER TARJETA' || 
								response.description === 'TARJETA BLOQUEADA' || response.description === 'TARJETA BLOQUEADA O PROBLEMAS CON LA TARJETA') {
							error = {
								msg: 'Card rejected. Data: ' + JSON.stringify(response),
								code: 'CardError' + ((response.code === 1)? '-fatal' : ''),
								data: response
							};
						} else
						if (response.description === 'EXCEDE MAXIMO' || response.description === 'MONTO EXCEDE EL MAXIMO') {
							error = {
								msg: 'Card exceeded. Data: ' + JSON.stringify(response),
								code: 'ExceededCardError' + ((response.code === 1)? '-fatal' : ''),
								data: response
							};
						} else
						if (response.description === 'NUMERO DE TARJETA ERRONEO/TARJETA NO TIENE HABILITADO RUBRO' || response.description === 'CAMBIE CLAVE ATM' ||
								response.description === 'CAMBIO DE TASAS' || response.description === 'CLAVE INVALIDA' || response.description === 'CUOTA INVALIDA' ||
								response.description === 'RECHAZADO CVV' || response.description === 'TARJETA INVALIDA' || response.description === 'TARJETA NO EXISTE EN EL MAESTRO' ||
								response.description === 'TARJETA VENCIDA' || response.description === 'TASA EXCEDE LA MAXIMA') {
							error = {
								msg: 'Card invalid. Data: ' + JSON.stringify(response),
								code: 'InvalidCardError',
								data: response
							};
						} else
						if (response.description === 'TARJETA Y NEGOCIO RECHAZADOS') {
							error = {
								msg: 'Card and business number rejected. Data: ' + JSON.stringify(response),
								code: 'CardBusinessError',
								data: response
							};
						} else
						if (response.description === 'TERMINO DE REINTENTOS DE PAGO') {
							error = {
								msg: 'Exceeded the maximum number of attempts. Data: ' + JSON.stringify(response),
								code: 'AttemptsError' + ((response.code === 1)? '-fatal' : ''),
								data: response
							};
						} else
						if (response.description === 'PAGO RECHAZADO' || response.description === 'DIRECCION LOGICA INVALIDA (TERMINAL ID)' ||
								response.description === 'REINTENTE' || response.description === 'SERVICIO INHABILITADO') {
							error = {
								msg: 'Payment rejected. Data: ' + JSON.stringify(response),
								code: 'PaymentError' + ((response.code === 1)? '-fatal' : ''),
								data: response
							};
						} else
						if (response.description === 'FALTAN DATOS EN EL CARGO') {
							error = {
								msg: 'Payment data missing. Data: ' + JSON.stringify(response),
								code: 'PaymentDataError' + ((response.code === 1)? '-fatal' : ''),
								data: response
							};
						} else
						if (response.description === 'PROBLEMAS CON RESPUESTA DE SERVICIO DE PAGO' || response.description === 'PROBLEMAS EN BASE DE DATOS') {
							error = {
								msg: 'Error checking booking. Data: ' + JSON.stringify(response),
								code: 'CheckError',
								data: response
							};
						} else
						if (response.description === 'CODIGO VENDEDOR NO CORRESPONDE AL USUARIO') {
							error = {
								msg: 'El negocio no corresponde al usuario firmado.',
								code: 'CreditLineError',
								data: response
							};
						} else
						if (response.description === 'EL CARGO ES MAYOR AL DISPONIBLE') {
							error = {
								msg: 'Saldo insuficiente en línea de crédito.',
								code: 'CreditLineError',
								data: response
							};
						} else
						if (/EL NEGOCIO .*? NO EXISTE EN SMART/.test(response.description)) {
							error = {
								msg: 'Negocio no creado en SMART.',
								code: 'CreditLineError',
								data: response
							};
						} else {
							error = {
								msg: 'Error on booking proccess. Obtained: ' + JSON.stringify(response),
								code: 'BridgeError',
								data: response
							};
						}
					} else {
						let status;
						if (response.description === 'DIFERENCIA DE PRECIOS') {
							status = 'PriceDifference';
						} else
						if (response.description === 'RESERVA DUPLICADA') {
							status = 'DuplicateReservation';
						} else
            if (response.description === 'RESERVA DUPLICADA') {
							status = 'DuplicateReservation';
						} else {
							status = 'Complete';
						}
						resp = {
							status: status + 'B2B',
							authPrice: (response.priceFlightHotel)? response.priceFlightHotel.totalCLP : null
						};
						validSteps.bookEnd = true;
					}
				} else {
					// ============= Temporal, hasta agregar nuevo manejo de errores
					if (_.get(response, 'message', null) && response.message === 'Tarjeta Rechazada') {
						error = {
							msg: 'Payment problems. Rejected Card. Data: ' + JSON.stringify(response),
							code: 'CardError',
							data: response
						};
					} else
					if (_.get(response, 'bookMessage', null) && response.bookMessage === 'ERROR! al reservar hotel') {
						error = {
							msg: 'Error on hotel availability or reservation. Data: ' + JSON.stringify(response),
							code: 'HotelError',
							data: response
						};
					} else
					if (!_.get(response, 'code', null)) {
						error = {
							msg: 'Bad formed response. Expected to have in data "code" property. Obtained: ' + JSON.stringify(response),
							code: 'BridgeError',
							data: response
						};
					} else
					if (response.code !== '00' && response.code !== '02') {
						error = {
							msg: 'Error booking (status: ' + response.status + '). Data: ' + JSON.stringify(response),
							code: 'BookingError',
							data: response
						};
					} else {
						let status = (response.code === '00' || response.code === '000')? 'Complete' : ((response.status === 'Diferencia de Precios')? 'PriceDifference' : 'DuplicateReservation');
						resp = {
							status: status + 'B2B',
							authPrice: (response.priceFlightHotel)? response.priceFlightHotel.totalCLP : null
						};
						validSteps.bookEnd = true;
					}
					// =============
				}
				break;
		}
		if (error){
			return false;
		}
	});
	return (error)? ({error: error}) : ({reponse: resp, progress: validSteps});
}

function* checkOpportunity() {
	let oportunities = yield oportunityModel.getPnrOportunities();

	let pnrStatus = {};
	let bookingRegisters = yield bookingLogModel.find({where: {and: [{or: oportunities.map((oportunity) => {return {spnr: oportunity.pnr};})} , {or: [{step: 'TARIFICACION_FLIGHT'}, {step: 'PACKAGE_BOOKING'}]}]}, order: 'spnr ASC', fields: {id: false, request: false, response: true, currentDate: false, spnr: true, step: true, provider: false}});
	_.each(bookingRegisters, (stage) => {
		if (!pnrStatus[stage.spnr]) {
			pnrStatus[stage.spnr] = {
				availabilityFlight: false,
				bookEnd: false
			};
		}

		if (stage.step === 'TARIFICACION_FLIGHT') {
			pnrStatus[stage.spnr].availabilityFlight = true;
		} else
		if (stage.step === 'PACKAGE_BOOKING' && !pnrStatus[stage.spnr].bookEnd) {
			let response = JSON.parse(stage.response);
			if (_.get(response, 'description', null) && response.description === 'RESERVA CONFIRMADA') {
				pnrStatus[stage.spnr].bookEnd = true;
			}
		}
	});
	for (let pnr in pnrStatus) {
		if (pnrStatus[pnr].availabilityFlight && !pnrStatus[pnr].bookEnd) {
			try {
				let config = yield mailParser.parseOportunity(yield oportunityModel.getOportunity((_.find(oportunities, {pnr: pnr})).email), pnr);
				sendMailService.sendMail(config, (err, result) => {
					if (err) {
						Koa.log.error(err);
					}
				});
			} catch (err) {
				Koa.log.error(err);
			}

			try {
				let conf = yield mailParser.parseOportunityOld(yield oportunityModel.getOportunity((_.find(oportunities, {pnr: pnr})).email), pnr);
				sendMailService.sendMail(conf, (err, result) => {
					if (err) {
						Koa.log.error(err);
					}
				});
			} catch (err) {
				Koa.log.error(err);
			}
		}
	}
}

function* waitRequest(minutes) {
	return new Promise((resolve, reject) => {
		externalRequest('https://mid.cocha.com/traveldeals/v1/wait-minutes/' + minutes, function(err, res, body){
			if(err){
				reject(err);
			} else {
				resolve({result: body});
			}
		});
	});
}

module.exports = {
	getFlightsWithHotels: getFlightsWithHotels,
	pricingFlightsWithHotels: pricingFlightsWithHotels,
	quoteFlightsWithHotels: quoteFlightsWithHotels,
	getHotelDetail: getHotelDetail,
	autocomplete: autocomplete,
	setBlacklist: setBlacklist,
	setRateRules: setRateRules,
	trackBooking: trackBooking,
	checkOpportunity: checkOpportunity,
	waitRequest: waitRequest
};

function getTransfer(_params, _workflowId) {
	let url = Koa.config.path.getTransfer;
	return new Promise((resolve, reject) => {
		webServices.get('flightshotels', url, _params, null, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject(err);
			} else {
				resolve(result);
			}
		}, _workflowId);
	});
}

function* searchHotels(_params, _workflowId){
	if(!_params){
		return false;//sonar
	}
	//let hotels = yield new Promise((resolve, reject) => {
	return yield new Promise((resolve, reject) => {
		hotelServices.getMetaList(_params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject(err);
			} else {
				resolve(result);
			}
		}, _workflowId);
	});
	//return yield hotelParser.parseHotels(hotels, _params);
}

function* getPointOfInterest(_params, _workflowId){
	return yield new Promise((resolve, reject) => {
		hotelServices.pointsOfInterest(_params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject(err);
			} else {
				resolve(result);
			}
		}, _workflowId);
	});
}

function* getHotelDetailInfo(_params, _workflowId){
	return yield new Promise((resolve, reject) => {
		hotelServices.getMetaDetail(_params, (err, result) => {
			if (err) {
				Koa.log.error(err);
				reject(err);
			} else {
				resolve(result);
			}
		}, _workflowId);
	});
}
