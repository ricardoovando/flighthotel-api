/**
 * FlightParser.js
 */

'use strict';
/* jshint strict: false, esversion: 6 */

let crypto = require('crypto');

function* parseFlights(_flights, _adt, _chd, _inf, _channel) {
	let totalPax = _adt + _chd + _inf;
	let rawFlightsPrices = {
		flightGroup: [],
		delta: {} 
	};
	let priceFeeB2b = {
			usd: 0,
			clp: 0
	};
	let dateArrivalFlight, dateLimitDayLess; //, dateLimitDayMore

	if (_channel === 'B2B') {
		priceFeeB2b.usd = 60;
		priceFeeB2b.clp = 60 * _flights.exchangeRate;
	}

	let cleanFlight = (_travels) => {
		var flightsKeys = {};
		_.each(_travels, (travel) => {
			// dateLimitDayMore = moment(travel.departure, 'YYYY-MM-DD').hour(6).format('YYYY-MM-DD HH:mm');
			dateLimitDayLess = moment(travel.departure, 'YYYY-MM-DD').hour(6).add(1, 'd').format('YYYY-MM-DD HH:mm');
			dateArrivalFlight = moment(travel.arrival, 'YYYY-MM-DDTHH:mm').format('YYYY-MM-DD HH:mm');
			/*
			if (dateArrivalFlight < dateLimitDayMore) {
				travel.arrivalType = 'early';
			} else
			*/
			if (dateArrivalFlight > dateLimitDayLess) {
				travel.arrivalType = 'late';
			} else {
				travel.arrivalType = 'just';
			}

			delete travel.ref;
			_.each(travel.connections, (connection) => {
				delete connection.extraParamsList;
			});		
			flightsKeys[travel.keyConnections] = travel.flightId;
		});
		return flightsKeys;
	};

	let parsePrice = (_prices, _currency, _debug) => {
		// let feeB2b = _prices.feeB2b[_currency];
		let feeB2b = priceFeeB2b[_currency];
		_prices = _prices.details[_currency];

		let base = _prices.ADT.base * _adt;
		let taxes = _prices.ADT.taxes * _adt;
		let fee = _prices.ADT.fee * _adt;

		if (_debug) {
			console.log("FLIGHT BASE TOTAL ADT", base);
			console.log("FLIGHT TAXES TOTAL ADT", taxes);
			console.log("FLIGHT FEE TOTAL ADT", fee);
		}

		if (_chd > 0) {
			base += (_prices.CHD.base * _chd);
			taxes += (_prices.CHD.taxes * _chd);
			fee += (_prices.CHD.fee * _chd);
			if (_debug) {
				console.log("FLIGHT BASE TOTAL CHD", (_prices.CHD.base * _chd));
				console.log("FLIGHT TAXES TOTAL CHD", (_prices.CHD.taxes * _chd));
				console.log("FLIGHT FEE TOTAL CHD", (_prices.CHD.fee * _chd));
			}
		}

		if (_inf > 0) {
			base += (_prices.INF.base * _inf);
			taxes += (_prices.INF.taxes * _inf);
			fee += (_prices.INF.fee * _inf);
			if (_debug) {
				console.log("FLIGHT BASE TOTAL INF", (_prices.INF.base * _chd));
				console.log("FLIGHT TAXES TOTAL INF", (_prices.INF.taxes * _chd));
				console.log("FLIGHT FEE TOTAL INF", (_prices.INF.fee * _chd));
			}
		}

		if (_debug) {
			console.log("FLIGHT BASE TOTAL", base);
			console.log("FLIGHT TAXES TOTAL", taxes);
			console.log("FLIGHT FEE TOTAL", fee);
			console.log("FLIGHT INDIVIDUAL RATE ", (base + taxes + fee) / totalPax);
		}

		return {
			totalBase: (base + taxes + fee) / totalPax,
			//totalTaxes: taxes / totalPax,
			//totalFee: fee / totalPax,
			totalFeeB2b: feeB2b / totalPax,
			currency: _prices.ADT.currency
		};
	};

	let moreRecommended = _flights.recommended;
	let recommendations = {};
	let recommendedPrice;
	if (moreRecommended) {		
		recommendedPrice = {
			usd: parsePrice(moreRecommended.prices, 'usd'),
			clp: parsePrice(moreRecommended.prices, 'clp')
		};
		rawFlightsPrices.delta = recommendedPrice;

		let minorTime, departure, returning;
		_.each(_flights.found, function(flight) {	

			flight.priceDetail = {
				usd: parsePrice(flight.prices, 'usd'),
				clp: parsePrice(flight.prices, 'clp')
			};
			flight.priceDetail.usd.totalBase -= recommendedPrice.usd.totalBase;
			flight.priceDetail.clp.totalBase -= recommendedPrice.clp.totalBase;

			rawFlightsPrices.flightGroup.push({
				priceBase: _.cloneDeep(flight.prices),
				priceRefactor: _.cloneDeep(flight.priceDetail),
				departures: cleanFlight(flight.departures),
				returnings: cleanFlight(flight.returnings),
			});

			// ===== DEPRECAR -> ======
			flight.prices = parsePrice(flight.prices, 'usd');
			flight.prices.totalBase -= recommendedPrice.usd.totalBase;
			//flight.prices.totalTaxes -= recommendedPrice.totalTaxes;
			//flight.prices.totalFee -= recommendedPrice.totalFee;
			// ===== <- DEPRECAR ======

			
			departure = _.minBy(flight.departures, 'totalDuration');
			returning = _.minBy(flight.returnings, 'totalDuration');
			if (minorTime) {
				if ((departure.totalDuration + returning.totalDuration) < (minorTime.departures[0].totalDuration + minorTime.returnings[0].totalDuration)) {
					minorTime.tripId = flight.tripId;
					minorTime.prices = flight.prices;
					minorTime.priceDetail = flight.priceDetail;
					minorTime.departures = [departure];
					minorTime.returnings = [returning];
				}
			} else {
				minorTime = {
					tripId: flight.tripId,
					prices: flight.prices,
					priceDetail: flight.priceDetail,
					departures: [departure],
					returnings: [returning]
				};
			}
		});

		delete moreRecommended.tripParent;
		delete moreRecommended.flightsInParent;
		// ===== DEPRECAR -> ======
		moreRecommended.prices = _.cloneDeep(recommendedPrice.usd);
		moreRecommended.prices.totalBase = 0;
		//moreRecommended.prices.totalTaxes = 0;
		//moreRecommended.prices.totalFee = 0;
		// ===== <- DEPRECAR ======
		
		moreRecommended.priceDetail = _.cloneDeep(recommendedPrice);
		moreRecommended.priceDetail.usd.totalBase = 0;
		moreRecommended.priceDetail.clp.totalBase = 0;

		recommendations.recommended = moreRecommended;

		recommendedPrice.id = crypto.createHmac('sha256', 'c0ch4d1g1t4l').update(Math.random().toString() + moment().unix()).digest("hex");
		
		let minorPrice = _.minBy(_flights.found, 'priceDetail.clp.totalBase');
		if (minorPrice && moreRecommended.departures[0].flightId !== minorPrice.departures[0].flightId && (moreRecommended.prices.totalBase > minorPrice.prices.totalBase)) {
			recommendations.lowerPrice = {
				tripId: minorPrice.tripId,
				prices: minorPrice.prices,
				priceDetail: minorPrice.priceDetail,
				departures: [minorPrice.departures[0]],
				returnings: [minorPrice.returnings[0]],
			};
		}

		if (minorTime && moreRecommended.departures[0].flightId !== minorTime.departures[0].flightId && ((moreRecommended.departures[0].totalDuration + moreRecommended.returnings[0].totalDuration) > (minorTime.departures[0].totalDuration + minorTime.returnings[0].totalDuration))) {
			recommendations.lowerTravelTime = minorTime;
		}
	}

	return yield {
		found: _flights.found,
		recommendations: recommendations,
		referencePrice: recommendedPrice,
		national: _flights.national,
		exchangeRate: _flights.exchangeRate,
		places: _flights.places,
		airlines: _flights.airlines,
		dataPrices: rawFlightsPrices
	};
}

module.exports = {
	parseFlights: parseFlights
};