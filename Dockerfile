FROM gcr.io/cocha-services/node-basecontainer

RUN mkdir -p /node-app
ADD . /node-app

WORKDIR /node-app
RUN npm install

EXPOSE 1337

CMD node app.js
